<?php

// General and meta configs
$config['site_title'] = "BumiDesa";
$config['site_tagline'] = "Pasar Online Komoditas Desa, Usaha Ekonomi Rakyat";
$config['site_description'] = "Temukan pemasok komoditas hasil bumi rakyat dan produk olahannya, dengan layanan pemasok yang mengikuti standar, pembayaran yang aman, nyaman dan terpercaya.";
$config['site_keywords'] = "komoditas, commodity, retail, wholesale, ritel, pemasok, suppliers, petani, farmers, bumdes, desa, village, exporter, expoters, eksportir, importir, importer, importers, grosir, kopi, coffee, gambir, gambier, uncaria gambir, karet, para, latex, lateks, wood, crafts, art, kerajinan, seni, singkong, cassava, arabica, arabica, robusta, cocoa, kakao, coklat, pala, lada, timun, tomat, wortel, daging, ayam, sapi, kambing, snack, cemilan, jeruk, durian, manggis, duku, rambutan, manga, buah tropis, tropical fruit, rempah, bumbu dapur, spices, souvenirs, oleh-oleh, tanda mata, kenang-kenangan, mainan anak, mainan edukasi, toys, educational toys, halal, furniture, meubel, mebel, kulit, leather, perhiasan, jewellery, makanan dalam kemasan, package food, herbal, batik, pakaian, sepatu, apparel, tekstil, textile, alat pertanian, farming tools, bibit, seeds, pupuk, fertilizer, insektisida , insecticide, pulsa, token, listrik, pasca bayar, pra bayar, tagihan, bill payment";

// Registration-related
$config['email_activation'] = false;

// Default Context used as Product List
$config['api_context'] 			= 'mp-master-wsproduct';
$config['api_context_trx'] 		= 'mp-master-wstrx';
$config['api_context_user'] 	= 'mp-master-wsuser';
$config['api_context_customer'] = 'cent-wscustomer';
$config['api_context_common'] 	= 'cent-wscommon';
$config['api_context_shipping'] = 'shipping-service';
$config['api_context_corporate'] = 'cent-am-ws';

// API Configs
$config['api_base_url'] 		= 'https://dev-api.cent91.com/' ;
$config['api_source'] 			= 'WEB';
$config['api_source_desc'] 		= 'bumidesa.com';