<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AjaxController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->output->set_content_type('application/json');
    }

    public function forward()
    {
        $params = $this->uri->segment_array();

        unset($params[0]);
        unset($params[1]);
        unset($params[2]);

        $this->api->set_context('');
        $data = $this->input->post();
        $source = $this->input->post('source') == '0' ? false : true;

        unset($data['source']);

        $return = $this->api->post(implode('/', $params), $data, $source);

        $this->output->set_output(json_encode($return));
    }


    public function shipping_product($id = 0)
    {
        $id = $this->input->get('id');
        $vendor = $this->input->get('vendor_id');

        $this->curl->httpHeader('X-Auth','bisnisq');
        $this->curl->httpHeader('X-Signature','4099039B5F047BF269B85A86C2A9B8B4B2E3809564F7E6299BA35662DADE3402');
        $shipping = $this->api->set_context($this->config->item('api_context_shipping'))->post('service/product/list',['companyId' => $id , 'vendorId' => $vendor],TRUE);

        $this->output->set_output(json_encode($shipping->rows));
    }

    public function shipping_rate()
    {
        $params = [
            'product_id' => $this->input->post('productId'),
            'origin' => $this->input->post('origin'),
            'destination' => $this->input->post('destination'),
        ];

        $this->curl->httpHeader('X-Auth','bisnisq');
        $this->curl->httpHeader('X-Signature','4099039B5F047BF269B85A86C2A9B8B4B2E3809564F7E6299BA35662DADE3402');
        $shipping = $this->api->set_context($this->config->item('api_context_shipping'))->post('service/rate/detail/list',$params,TRUE);

        if (!empty($shipping->rows)) {
            $this->output->set_output(json_encode($shipping->rows[0]));
        } else {
            $this->output->set_output(json_encode(['rate' => 0]));
        }
    }

    public function wishlist()
    {
        $product_id = $this->input->post('product_id');
        $trx_id = $this->input->post('trx_id');
        $action = $this->input->post('action');

        $product = $this->api->post('service/product/detail', ['productId' => $product_id]);

        if ($product->errorCode != 0){
            $this->output->set_output(json_encode(['status' => 0, 'msg' => 'Product not found.']));
        }

        if ($action == 'add') {
            $sku = (isset($product->skuList[0][0])) ? $product->skuList[0][0] : '-';
            $city = (isset($product->vendorResponse->city)) ? $product->vendorResponse->city : '-';

            $params = [
                'productId' => $product_id,
                'skuId' => $sku,
                'cityId' => $city,
                'quantity' => 1
            ];

            $wish = $this->api->set_context($this->config->item('api_context_trx'))->post('service/wishlist/add',$params);

            if ($wish->errorCode  == 0) {
                $this->output->set_output(json_encode([
                    'status' => 1,
                    'msg' => 'Produk berhasil ditambahkan ke wishlist',
                    'data' => $wish
                ]));
            } else {
                $this->output->set_output(json_encode([
                    'status' => 0,
                    'msg' => 'Terjadi Kesalahan Teknis',
                ]));
            }

        } elseif ($action == 'remove') {

            $wish = $this->api->set_context($this->config->item('api_context_trx'))->post('service/wishlist/remove/item',["trxItemId" => $trx_id]);

            if ($wish->errorCode  == 0) {
                $this->output->set_output(json_encode(['status' => 1, 'msg' => 'Produk berhasil dihapus dari wishlist']));
            } else {
                $this->output->set_output(json_encode(['status' => 0, 'msg' => 'Terjadi Kesalahan Teknis']));
            }
        }
    }
}
