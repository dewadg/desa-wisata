<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
    }

    /**
     * Displays login page.
     *
     * @return void
     */
    public function loginPage()
    {
        $this->load->view('customer/parts/header');
        $this->load->view('customer/login');
        $this->load->view('customer/parts/footer');
    }

    /**
     * Authenticates a user.
     *
     * @return void
     */
    public function login()
    {
        if ($this->vauth->logged_in()) {
            redirect('/');
        }

        $this->form_validation->set_rules(array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|trim',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
            ),
        ));

        if (! $this->form_validation->run()) {
            $this->session->set_flashdata('errors', $this->form_validation->error_array());

            redirect('/login');
        }

        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);

        if ($this->vauth->login($email, $password)) {
            if ($this->input->get('redirect')) {
                redirect($this->input->get('redirect'));
            }

            redirect('/');
        } else {
            $this->session->set_flashdata('errors', $this->vauth->error());

            redirect('login?' . http_build_query($this->input->get()));
        }
    }

    /**
     * Logouts a user.
     *
     * @return void
     */
    public function logout()
    {
        $this->vauth->logout();

        redirect('/');
    }
}
