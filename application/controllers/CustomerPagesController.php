<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerPagesController extends CI_Controller
{
	/**
	 * Displays landing page.
	 *
	 * @return void
	 */
	public function indexPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/landing');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays about page.
	 *
	 * @return void
	 */
	public function aboutPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/about');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays payment page.
	 *
	 * @return void
	 */
	public function paymentPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/payment');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays transaction profits page.
	 *
	 * @return void
	 */
	public function transactionProfitsPage()
	{
		$this->load->view('customer/parts/header');
		// TODO: Create specific transaction profit page view.
		$this->load->view('customer/about');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays how to rent page.
	 *
	 * @return void
	 */
	public function howToRentPage()
	{
		$this->load->view('customer/parts/header');
		// TODO: Create specific how to rent page view.
		$this->load->view('customer/about');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays how to book page.
	 *
	 * @return void
	 */
	public function howToBookPage()
	{
		$this->load->view('customer/parts/header');
		// TODO: Create specific how to book page view.
		$this->load->view('customer/about');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays transaction flow page.
	 *
	 * @return void
	 */
	public function transactionFlowPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/transaction-flow');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays transaction security page.
	 *
	 * @return void
	 */
	public function transactionSecurityPage()
	{
		$this->load->view('customer/parts/header');
		// TODO: Create specific transaction security page view.
		$this->load->view('customer/about');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays terms of service page.
	 *
	 * @return void
	 */
	public function tosPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/tos');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays privacy policy page.
	 *
	 * @return void
	 */
	public function privacyPolicyPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/privacy-policy');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays help page.
	 *
	 * @return void
	 */
	public function helpPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/help');
		$this->load->view('customer/parts/footer');
	}

	/**
	 * Displays contact page.
	 *
	 * @return void
	 */
	public function contactPage()
	{
		$this->load->view('customer/parts/header');
		$this->load->view('customer/contact');
		$this->load->view('customer/parts/footer');
	}
}
