<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RegistrationController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
    }

    /**
     * Displays registration page.
     *
     * @return void
     */
    public function registrationPage()
    {
        if ($this->vauth->logged_in()) {
            redirect('home');
        }

        $this->api->set_context($this->config->item('api_context_common'));
        $data['provinces'] = $this->api->post('/service/province/list', [], true)->list;

        $this->load->view('customer/parts/header');
        $this->load->view('customer/registration/form', $data);
        $this->load->view('customer/parts/footer');
    }

    /**
     * Register a customer.
     *
     * @return void
     */
    public function register()
    {
        if ($this->vauth->logged_in()) {
            redirect('home');
        }

        $this->form_validation->set_rules(array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email|trim',
            ),
            array(
                'field' => 'name',
                'label' => 'Nama',
                'rules' => 'required',
            ),
            array(
                'field' => 'phone',
                'label' => 'No. Handphone',
                'rules' =>  array(
                    'required',
                    array(
                        'valid_phone',
                        function ($val) {
                            return phone_validation($val);
                        }
                    )
                ),
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]|matches[password_conf]',
            ),
            array(
                'field' => 'password_conf',
                'label' => 'Konfirmasi Password',
                'rules' => ($this->input->post('password')) ? 'required' : '',
            ),
            array(
                'field' => 'cityId',
                'label' => 'Kota / Kabupaten',
                'rules' => 'required|is_natural',
            ),
            array(
                'field' => 'postalCode',
                'label' => 'Kode Pos',
                'rules' => 'required|is_natural|exact_length[5]|trim',
            ),
            array(
                'field' => 'address',
                'label' => 'Alamat',
                'rules' => 'required',
            ),
        ));

        $this->form_validation->set_message('valid_phone', 'No. HP tidak valid. Isi dengan format +628xxxxxxxx');

        if ($this->form_validation->run()) {
            $register_data = [
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'password' => hash('sha256', $this->input->post('password')),
                'address' => $this->input->post('address'),
                'cityId' => $this->input->post('cityId'),
                'postalCode' => $this->input->post('postalCode'),
                'referenceCode' => $this->input->post('referenceCode'),
                'emailAktifasi' => 'no',
                'urlActv' => '-'
            ];

            // TODO: implement email activation

            if ($this->vauth->register($register_data)) {
                $this->session->set_flashdata('registration-success', true);
                redirect('/register/success');
            } else {
                $this->session->set_flashdata('errors', $this->vauth->error());
                redirect('/register');
            }
        } else {
            $this->session->set_flashdata('errors', $this->form_validation->error_array());
            redirect('/register');
        }
    }

    /**
     * Displays registration success page.
     *
     * @return void
     */
    public function registrationSuccessPage()
    {
        if ($this->vauth->logged_in()) {
            redirect();
        }

        if (is_null($this->session->flashdata('registration-success'))) {
            redirect('login');
        }

        $this->load->view('customer/parts/header');
        $this->load->view('customer/registration/success');
        $this->load->view('customer/parts/footer');
    }
}