<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('generate_csrf_field')) {

    function generate_csrf_field(){

        $CI =& get_instance();

        $result = '<input type="hidden" name="' .$CI->security->get_csrf_token_name(). '" value="' .$CI->security->get_csrf_hash(). '">';

        return $result;
    }
}

if (! function_exists('csrf_token')) {

    function csrf_token($type= 'hash'){

        $CI =& get_instance();

        if ($type == 'name') {
            return $CI->security->get_csrf_token_name();
        }

        return $CI->security->get_csrf_hash();

    }
}


if (! function_exists('vue')) {

    function vue($output = null){

        return "{{" . $output . "}}";

    }
}




if (! function_exists('is_wish')) {

    function is_wish($product_id){

        $CI =& get_instance();

        if (!$CI->vauth->logged_in()) return false;


        foreach ($GLOBALS['wishlist'] as $key => $value) {
            if ($value->productDetail->id == $product_id) {
                return $value->trxItemId;
            }
        }

        return false;

    }
}

if (! function_exists('generate_breadcrumb')) {

    function generate_breadcrumb($data){

        if (empty($data)) {

            return NULL;
        }

        $result ='<ol class="breadcrumb">';

        foreach ($data as $uri => $item) {
            if ($uri == '!end!') {
                $result .= '<li class="active">' .$item. '</li>';

                continue;
            }

            $result .= '<li><a href="' .site_url($uri). '">' .$item. '</a></li>';

        }

        $result .= '</ol>';

        return $result;
    }
}

if (! function_exists('generate_pagination')) {

    function generate_pagination($base_url, $total_data, $limit){

        $CI =& get_instance();

        $CI->load->library('pagination');

        $config = get_pagination_config();

        // set pagitanion variable
        $offset = ($CI->input->get($config['query_string_segment']) > 1)
            ? ( ($CI->input->get($config['query_string_segment']) - 1) * $limit)
            : 0;

        // set config
        $config['base_url'] = $base_url;
        $config['total_rows'] = $total_data;
        $config['per_page'] = $limit;

        $CI->pagination->initialize($config);

        // set data
        $counter = $offset + 1;
        $link = $CI->pagination->create_links();

        return $link;
    }

    function get_pagination_config(){

        // Customizing the Pagination
        $config['uri_segment']					= 3;				// default: 3
        $config['num_links']					= 2;				// default: 2
        $config['prefix']						= '';				// default: ''
        $config['suffix']						= '';				// default: ''
        $config['use_global_url_suffix']		= FALSE;			// default: FALSE

        // Customizing query string format
        $config['use_page_numbers']				= TRUE;				// default: TRUE
        $config['page_query_string']			= TRUE;				// default: TRUE
        $config['enable_query_strings']			= TRUE;				// default: FALSE
        $config['query_string_segment']			= 'page';				// default: 'per_page'
        $config['reuse_query_string']			= TRUE;				// default: FALSE

        // Adding Enclosing Markup
        $config['full_tag_open']				= '<ul class="pagination">';			// default: '<p>'
        $config['full_tag_close']				= '</ul>';			// default: '</p>'

        // Customizing the First Link
        $config['first_link']					= '&laquo;';		// default: 'First'
        $config['first_tag_open']				= '<li>';			// default: '<div>'
        $config['first_tag_close']				= '</li>';			// default: '</div>'
        $config['first_url']					= '';				// default: ''

        // Customizing the Last Link
        $config['last_link']					= '&raquo;';		// default: 'Last'
        $config['last_tag_open']				= '<li>';			// default: '<div>'
        $config['last_tag_close']				= '</li>';			// default: '</div>'

        // Customizing the "Next" Link
        $config['next_link']					= '&gt;';			// default: '&gt;'
        $config['next_tag_open']				= '<li>';			// default: '<div>'
        $config['next_tag_close']				= '</li>';			// default: '</div>'

        // Customizing the "Previous" Link
        $config['prev_link']					= '&lt;';			// default: '&lt;'
        $config['prev_tag_open']				= '<li>';			// default: '<div>'
        $config['prev_tag_close']				= '</li>';			// default: '</div>'

        // Customizing the "Current Page" Link
        $config['cur_tag_open']					= '<li class="active"><a href="#" onclick="return false;">';	// default: '<b>'
        $config['cur_tag_close']				= '</a></li>';		// default: '</b>'

        // Customizing the "Digit" Link
        $config['num_tag_open']					= '<li>';			// default: '<div>'
        $config['num_tag_close']				= '</li>';			// default: '</div>'

        // Hiding the Pages
        $config['display_pages']				= TRUE;				// default: FALSE

        // Adding attributes to anchors
        $config['attributes']					= array();			// default: array()

        // Disabling the "rel" attribute
        $config['attributes']['rel']			= FALSE;			// default: FALSE

        return $config;
    }
}

if (! function_exists('generate_field_code')) {

    function generate_field_code($code){

        $result = $code . '-' . date('s') . date('y') . date('i') . date('m') . date('h') . date('d') . mt_rand(10, 99);

        return $result;
    }
}

if (! function_exists('reuse_query_string')) {

    function reuse_query_string($index = array()){

        $CI =& get_instance();

        if (empty($index)) {

            $result = ($query = $_SERVER['QUERY_STRING']) ? '?' . $query : NULL;
        }
        elseif(is_array($index)){

            $query = array();

            foreach ($index as $item) {

                $query[$item] = $CI->input->get($item);
            }

            $result = http_build_query($query);

            $result = (! empty($result)) ? '?' . $result : NULL;
        }
        elseif(is_string($index)){

            $result = ($CI->input->get($index) ) ? '?' .http_build_query(array($index => $CI->input->get($index))) : NULL;
        }

        return $result;
    }
}

if (! function_exists('currency')) {
    function currency($amount){
        return 'Rp '.number_format($amount,0,',','.');
    }
}


if (! function_exists('phone_validation')) {

    function phone_validation($val){

        $phone = str_replace(substr($val, 0,1), '', $val);

        //check minimum and start
        if (trim(strlen($val)) >= 10 &&  substr($val, 0,1) == '+' && ctype_digit($phone)) {
            return TRUE;
        }

        return FALSE;
    }
}