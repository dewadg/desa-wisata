<?php

if (! function_exists('isLogged')) {
    function isLogged()
    {
        $CI = &get_instance();

        return ! is_null($CI->session->userdata('userdata'))
            && $CI->session->userdata('logged_in') == 1;
    }
}
