<?php

if (! function_exists('isLandingPage')) {
    function isLandingPage() {
        $CI = &get_instance();

        return $CI->router->fetch_class() == 'CustomerController'
            && $CI->router->fetch_method() == 'index';
    }
}
