<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api
{
    private $error;

    protected $base_url;
    protected $api_context;

    /**
     * @var \GuzzleHttp\Client
     */
    // protected $guzzle;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('curl');

        // $this->guzzle = new \GuzzleHttp\Client([
        //    'base_uri' => $this->CI->config->item('api_base_url'),
        //    'headers' => [
        //        'Content-Type' => 'application/json',
        //    ],
        // ]);

        $this->base_url = $this->CI->config->item('api_base_url');
        $this->api_context = $this->CI->config->item('api_context');
        $this->api_source = $this->CI->config->item('api_source');
        $this->api_source_desc = $this->CI->config->item('api_source_desc');

    }

    public function set_context($context)
    {
        $this->api_context = $context;

        return $this;
    }

    public function set_base_url($url)
    {
        $this->base_url = $url;
    }

    public function get_url()
    {
        if ($this->api_context != '') {
            return $this->base_url . $this->api_context . '/';
        }

        return $this->base_url;
    }

    public function post($endpoint, $params = [], $include_source = false)
    {
        if ($include_source) {
            $params['source'] = $this->api_source;
            $params['sourceDesc'] = $this->api_source_desc;
        }

        $this->CI->curl->httpHeader('Content-Type','application/json');

        if ($this->CI->vauth->logged_in()) {
            $this->CI->curl->httpHeader('token', $this->CI->vauth->data('token'));
        }

        $result = $this->CI->curl->simple_post($this->get_url() . $endpoint, json_encode($params));

        return json_decode($result);

        /** Guzzle usage (for error checking). */
        //
        // $result = $this->CI->curl->simple_post($this->get_url() . $endpoint, json_encode($params));
        //
        // $response = $this->guzzle->post($this->get_url() . $endpoint, [
        //     'json' => $params,
        // ]);
        //
        // if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
        //    return json_decode((string) $response->getBody());
        // }
        //
        // return $response->getStatusCode();
    }
}
