<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vauth
{
    private $error;
    public $userdata;

    public function __construct()
    {
        $this->CI = &get_instance();

        if ($this->logged_in()) {
            $this->userdata = $this->CI->session->userdata('userdata');
        }
    }

    public function login($email, $password)
    {
        $this->CI->api->set_context($this->CI->config->item('api_context_customer'));

        //Request OTK
        $otk = $this->CI->api->post('/service/requestOtk', ['email' => $email]);

        if ($otk->errorCode != 0) {
            $this->error = 'Email yang anda masukkan salah atau belum aktif.';

            return false;
        }

        //Hash Password
        $password = hash('sha256', hash('sha256',$password) . $otk->otk );

        //Login
        $login = $this->CI->api->post('/service/login', [
            'email' => $email,
            'password' => $password,
        ], true);

        if ($login->errorCode  != 0) {
            $this->error = 'Password yang anda masukkan salah';

            return false;
        } else {
            $this->CI->curl->httpHeader('token',$login->token);

            $corporate = $this->CI->api
                ->set_context($this->CI->config->item('api_context_customer'))
                ->post('service/account/corporate/profile');

            if ($corporate->errorCode == '0') {
                $login->is_corporate = TRUE;
                $login->corporate = $corporate->corporate;
            } else {
                $login->is_corporate = FALSE;
            }

            $this->CI->session->set_userdata('logged_in', 1);
            $this->CI->session->set_userdata('userdata', $login);

            return true;
        }
    }

    public function register($params = [])
    {
        $this->CI->api->set_context($this->CI->config->item('api_context_customer'));
        $register = $this->CI->api->post( 'service/reg/register', $params , true);

        if ($register->errorCode != 0) {
            $this->error = $register->errorMsg;

            if ($register->errorCode == '11001') {
                $this->error = "Email atau No.HP Sudah digunakan user lain.";
            }

            return false;
        } else {
            return true;
        }
    }

    public function logout()
    {
        $this->CI->session->unset_userdata('logged_in');
        $this->CI->session->unset_userdata('userdata');
        $this->CI->session->unset_userdata('activeVendor');

        return true;
    }

    public function activation($email, $code)
    {
        $this->CI->api->set_context($this->CI->config->item('api_context_customer'));

        $reg = $this->CI->api->post('service/reg/activation', [
            'email'     => $email,
            'actvKey'   => $code,
            'actvFrom'  => $this->CI->config->item('api_source'),
        ]);

        if ($reg->errorCode == 0) {
            return true;
        } else {
            $this->error = $reg->errorMsg;

            return false;
        }
    }

    public function resend_activation($email, $url)
    {
        $this->CI->api->set_context($this->CI->config->item('api_context_customer'));

        $reg = $this->CI->api->post('service/reg/activation/resendcode', [
            'email'     => $email,
            'urlActv'  => $url,
        ]);

        if ($reg->errorCode == 0) {
            return true;
        } else {
            $this->error = $reg->errorMsg;

            return false;
        }
    }

    public function logged_in()
    {
        if ($this->CI->session->userdata('logged_in')) {
            return true;
        }

        return false;
    }

    public function data($key = null)
    {
        if ($key == null) return $this->userdata;

        if (isset($this->userdata->$key)) {
            return $this->userdata->$key;
        }

        return null;
    }

    public function error()
    {
        return $this->error;
    }
}
