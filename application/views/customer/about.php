<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center display-3 text-primary">DesaWisata</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Membangun Ekonomi Digital Dari Desa</h1>
			</div>
		</div>
	</div>
</div>
<div class="bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/visi.png">
					</div>
					<div class="col-sm-9">
						<h3 class="">Visi</h3>
						<p class="">Masyarakat Desa Menuju Pasar Global</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/misi.png">
					</div>
					<div class="col-sm-9">
						<h3 class="">Misi</h3>
						<p class="">Menyediakan Platform Badan Usaha Milik Desa (BUMDES) Untuk Bersaing di Pasar Global</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Apa itu DesaWisata?</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7 align-self-center">
				<h3>Bagi Pembeli</h3>
				<p class="my-3">DesaWisata memberikan Anda banyak pilihan hasil produksi desa langsung dari sumbernya. Berbagai
					produk dapat Anda temukan di DesaWisata (kami secara terus-menerus memperluas dan memperbanyak cakupan pemasok dan
					produknya).</p>
			</div>
			<div class="col-md-5">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/buyer.jpg"> </div>
		</div>
		<div class="row">
			<div class="col-md-5 order-2 order-md-1">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/seller.jpg"> </div>
			<div class="col-md-7 order-1 order-md-2">
				<h3>Bagi Pemasok</h3>
				<p class="my-3">DesaWisata membantu Anda memasarkan hasil produksi Anda secara luas di tingkat Nasional maupun
					Internasional.</p>
			</div>
		</div>
	</div>
</div>
<div class="py-5 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Mengapa saya memilih DesaWisata?</h1>
			</div>
		</div>
		<div class="row">
			<div class="py-5 col-md-6">
				<div class="row">
					<div class="text-center col-4">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok.png">
					</div>
					<div class="col-8">
						<h5 class="mb-3 text-primary">Akurat dan Terkini</h5>
						<p class="my-1">DesaWisata memberikan informasi tentang produk dan pemasoknya secara rinci dan akurat, langsung
							dari sumbernya.</p>
					</div>
				</div>
			</div>
			<div class="py-5 col-md-6">
				<div class="row">
					<div class="text-center col-4">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok.png">
					</div>
					<div class="col-8">
						<h5 class="mb-3 text-primary">
							<b>Aman dan Terpercaya</b>
						</h5>
						<p class="my-1">DesaWisata bekerja sama dengan mitra-mitra yang terpercaya untuk memastikan keamanan transaksi
							Anda.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="py-5 col-md-6">
				<div class="row">
					<div class="text-center col-4">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok.png">
					</div>
					<div class="col-8">
						<h5 class="mb-3 text-primary">
							<b>Mudah dan Nyaman</b>
						</h5>
						<p class="my-1">DesaWisata memudahkan Anda menemukan kebutuhan Anda langsung dari sumbernya.</p>
					</div>
				</div>
			</div>
			<div class="py-5 col-md-6">
				<div class="row">
					<div class="text-center col-4">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok.png">
					</div>
					<div class="col-8">
						<h5 class="mb-3 text-primary">
							<b>Standar Produk dan Pelayanan</b>
						</h5>
						<p class="my-1">DesaWisata berusaha memberikan standar produk dan pelayanan dengan melakukan seleksi terhadap
							pemasok dan produknya secara ketat.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Mengapa DesaWisata berbeda?</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok2.png">
					</div>
					<div class="col-sm-9">
						<h3 class="">B2C vs B2B</h3>
						<p class="">Selain melayani pemesanan untuk konsumen langsung, DesaWisata juga melayani pembelian bahan baku untuk
							pada produsen.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok2.png"> </div>
					<div class="col-sm-9">
						<h3 class="">Retail vs Grosir</h3>
						<p class="">DesaWisata bisa melayani pembelian baik dalam jumlah kecil maupun dalam partai besar.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok2.png"> </div>
					<div class="col-sm-9">
						<h3 class="">Cara Bayar</h3>
						<p class="">Selain pembayaran dengan cara yang lazim di situs pasar online lainnya, DesaWisata juga memiliki cara
							bayar yang cocok untuk transaksi B2B.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ok2.png">
					</div>
					<div class="col-sm-9">
						<h3 class="">Shipping</h3>
						<p class="">PIlihan pengiriman pesanan yang tersedia di DesaWisata sangat fleksibel dan bervariasi, menyesuaikan
							jarak, jumlah pesanan dan tingkat urgensi.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Apa yang bisa Anda temukan di DesaWisata?</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/hasil_bumi.jpg">
				<h3 class="my-2">Hasil Bumi</h3>
				<p class="">Berbagai produk hasil bumi komoditas, buah-buahan, dan sebagainya.</p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/hasil_olahan.jpeg">
				<h3 class="my-2">Hasil Olahan</h3>
				<p class="">Berbagai produk olahan dengan bahan baku dari hasil bumi.</p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/kerajinan_rakyat.jpeg">
				<h3 class="my-2">Kerajinan Rakyat</h3>
				<p class="">Berbagai produk kerajinan, baik yang tradisional maupun modern.</p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/bahan_baku_produksi.jpeg">
				<h3 class="my-2">Bahan Baku Produksi</h3>
				<p class="">Berbagai produk bahan baku produksi.</p>
			</div>
		</div>
	</div>
</div>
<div class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Siapa Mitra kami?</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ri.png"> </div>
					<div class="col-sm-9">
						<h3 class="">Pemerintah Indonesia</h3>
						<p class="">Dengan dukungan dari Kemendes dan Pemerintah Daerah, DesaWisata berusaha membangun jaringan yang aman
							bagi transaksi Anda.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 p-4">
				<div class="row">
					<div class="col-sm-3 text-center">
						<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/bumdes.png"> </div>
					<div class="col-sm-9">
						<h3 class="">Badan Usaha Milik Desa (BUMDes)</h3>
						<p class="">BUMDes merupakan badan usaha yang legal dan terpercaya, memberikan Anda keamanan dalam bertransaksi.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center">Mengapa BUMDes?</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/ekonomi_kerakyatan.png">
				<h3 class="my-2">Ekonomi Kerakyatan</h3>
				<p class="">BUMDes adalah salah satu wadah untuk meningkatkan Ekonomi Kerakyatan</p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/dari_masyarakat.jpg">
				<h3 class="my-2">Dari Masyarakat</h3>
				<p class="">BUMDes dibentuk oleh masyarakat dengan dukungan dari pemerintah</p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/bankable.png">
				<h3 class="my-2">Bankable</h3>
				<p class="">BUMDes didukung secara finansial oleh berbagai lembaga keuangan yang kuat dan terpercaya.</p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<img class="img-fluid d-block" src="https://image.bumidesa.com/contentstatic/legal_aman_terpercaya.png">
				<h3 class="my-2">Legal, Aman, Terpercaya</h3>
				<p class="">Berdirinya BUMDes dilandasi oleh UU No. 32 Tahun 2004 tentang Pemerintahan Daerah</p>
			</div>
		</div>
	</div>
</div>
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
 crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
 crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
 crossorigin="anonymous"></script> -->
