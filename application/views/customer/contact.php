<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="section-red-bg py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 text-white text-center">
				<h1>Hubungi Kami</h1>
				<h5>Apabila Anda membutuhkan bantuan kami, silahkan tinggalkan pesan di formulir berikut ini:</h5>
			</div>
		</div>
	</div>
</section>

<div class="py-5 bg-white">
	<div class="container py-3">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<form method="post">
					<input type="hidden" name="desawisata_csrf" value="7a5ade47e99a9f9bb5c4f72b51a96dbf">
					<div class="row">
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label>Nama Anda</label>
								<input value="" type="text" name="name" class="form-control" placeholder="Jane Doe">
								<small class="text-muted form-text">Cantumkan nama Anda agar kami lebih mengenal Anda.</small>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label>Alamat Email</label>
								<input value="" type="email" name="email" class="form-control" placeholder="abc@email.com">
								<small class="form-text text-muted">Kami tidak membagikan alamat email Anda kepada siapapun.</small>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label>Nomor Telepon/Handphone</label>
								<input value="" type="text" name="phone" class="form-control" placeholder="+628xxxxxxxxx">
								<small class="text-muted form-text">Sertakan Nomor telepon atau handphone yang dapat dihubungi agar kami dapat
									menghubungi Anda bila diperlukan.</small>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label>Topik</label>
								<div class="row">
									<div class="col-6">
										<div class="form-check">
											<label class="form-check-label">
												<input class="form-check-input" type="radio" name="topic" value="Keanggotaan" checked="">
												Keanggotaan</label>
										</div>
										<div class="form-check">
											<label class="form-check-label">
												<input class="form-check-input" type="radio" name="topic" value="Transaksi">
												Transaksi</label>
										</div>
										<div class="form-check">
											<label class="form-check-label">
												<input class="form-check-input" type="radio" name="topic" value="Kemitraan">
												Kemitraan</label>
										</div>
									</div>
									<div class="col-6">
										<div class="form-check">
											<label class="form-check-label">
												<input class="form-check-input" type="radio" name="topic" value="Fasilitas">
												Fasilitas</label>
										</div>
										<div class="form-check">
											<label class="form-check-label">
												<input class="form-check-input" type="radio" name="topic" value="Lain-lain">
												Lain-lain</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Pesan Anda kepada kami</label>
						<textarea class="form-control" name="message" value="" id="exampleTextarea" rows="10"></textarea>
					</div>
					<button type="submit" class="btn btn-primary btn-block" style="min-width:300px;">Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>
