<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="section-red-bg py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 text-white text-center">
				<h1>Pusat Bantuan</h1>
				<h5>Kami sadar bahwa tidak ada sistem yang sempurna. Oleh karena itu kami berusaha untuk membantu Anda
					menyelesaikan permasalahan yang ditemui di DesaWisata melalui halaman ini. Silahkan temukan solusinya atau bisa
					menghubungi kami dengan mengirimkan email ke cs@desawisata.com, atau melalui halaman Hubungi Kami.</h5>
			</div>
		</div>
	</div>
</section>

<div class="py-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="mb-3  text-center">Bantuan untuk Pendaftaran</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Pendaftaran sebagai Pembeli</h3>
				<p class=""></p>
			</div>
			<div class="col-md-6 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Pendaftaran sebagai Penjual</h3>
				<p class=""></p>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="mb-3  text-center">Bantuan untuk Keanggotaan</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Keanggotaan Individu</h3>
				<p class=""></p>
			</div>
			<div class="col-md-4 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Keanggotaan Institusi/Korporasi</h3>
				<p class=""></p>
			</div>
			<div class="col-md-4 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Keanggotaan Khusus BUMDes</h3>
				<p class=""></p>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="mb-3  text-center">Bantuan untuk Transaksi</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Pesanan</h3>
				<p class=""></p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Pembayaran</h3>
				<p class=""></p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Pengiriman</h3>
				<p class=""></p>
			</div>
			<div class="col-md-3 align-self-center text-center p-4">
				<i class="d-block  fa fa-5x fa-font-awesome"></i>
				<h3 class="text-secondary my-3">Pengaduan</h3>
				<p class=""></p>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="mb-3  text-center">Pertanyaan yang sering diajukan</h2>
				<div class="col-md-12">
					<ul class="list-group">
						<li class="list-group-item d-flex justify-content-between align-items-center">Pertanyaan seputar Login dan
							Password </li>
						<li class="list-group-item d-flex justify-content-between align-items-center">Pertanyaan seputar Email dan Nomor
							Handphone </li>
						<li class="list-group-item d-flex justify-content-between align-items-center">Pertanyaan seputar Pembayaran </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
