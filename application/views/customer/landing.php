<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section>
	<div class="home-slider m-0">
		<div class="banner" style="background-image: url('../assets/images/banner/comingsoon.jpg')">
			<div class="overlay"></div>
			<div class="container py-5 text-white px-5 mt-3">
				<h2>DesaWisata</h2>
			</div>
		</div>
		<div class="banner" style="background-image: url('../assets/images/banner/banner.jpg')">
			<div class="overlay"></div>
			<div class="container py-5 text-white px-5 mt-3">
				<h2>Teluk Mandeh</h2>
				<h4>Sumatra Barat</h4>
				<p>321 Penginapan.</p>
				<a href="product.php" class="btn btn-primary">CEK PENGINAPAN</a>
			</div>
		</div>
	</div>
</section>

<section class="bg-white" style="margin-top:-50px;">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-11">
				<div class=" card card-rounded px-3 pt-2 pb-1 bg-white">
					<div class="row no-gutters" id="hotel-form">
						<div class="col-12 col-md-3 pr-1">
							<div class="form-group select2-input">
								<label for="">Kota atau Tujuan Wisata</label>
								<select name="destination" id="hotelSearch" class="form-control">
									<option value="" selected=""></option>
									<option value="1">Padang</option>
									<option value="1">Medan</option>
									<option value="1">Bogor</option>
									<option value="1">Sukabumi</option>
								</select>
								<input type="hidden" name="search" value="">
							</div>
						</div>
						<div class="col-12 col-md-3 pl-1 d-flex input-daterange" date-range>
							<div class="form-group">
								<label for="">Check in</label>
								<input autocomplete="off" name="checkin" value="<?php echo date('m/d/Y') ?>" type="text" class="form-control check-in rounded-left">
							</div>
							<div class="form-group">
								<label for="">Check Out</label>
								<input autocomplete="off" name="checkout" value="<?php echo date('m/d/Y', strtotime('+1 day')) ?>" type="text"
								 class="form-control check-out rounded-right">
							</div>
						</div>
						<div class="col-12 col-md-3 px-1">
							<div class="form-group">
								<label for="">Tamu dan Kamar</label>
								<input data-toggle="dropdown" type="text" class="form-control" readonly="" :value="room + ' Kamar, ' + adult + ' Tamu'"
								 style="background-color: #fff">
								<div class="dropdown-menu w-100 p-3" role="menu" aria-labelledby="menu1">
									<div class="form-group">
										<div class="row">
											<div class="col-5">
												<label class="m-2">Kamar</label>
											</div>
											<div class="col-7">
												<div class="input-group">
													<div class="input-group-prepend">
														<button v-on:click="room--" type="button" class="btn btn-light"><i class="fa fa-minus"></i></button>
													</div>
													<input type="number" name="room" v-model="room" class="form-control text-center no-spinner qty-input">
													<div class="input-group-append">
														<button v-on:click="room++" type="button" class="btn btn-light"><i class="fa fa-plus"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-5">
												<label class="m-2">Tamu</label>
											</div>
											<div class="col-7">
												<div class="input-group">
													<div class="input-group-prepend">
														<button v-on:click="adult--" type="button" class="btn btn-light"><i class="fa fa-minus"></i></button>
													</div>
													<input name="adult" v-model="adult" type="number" min="1" value="1" class="form-control text-center no-spinner qty-input">
													<div class="input-group-append">
														<button v-on:click="adult++" type="button" class="btn btn-light"><i class="fa fa-plus"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-md pl-1">
							<label class="invisible">Submit</label>
							<a href="product.php" class="btn btn-primary btn-block"><i class="lnr lnr-magnifier"></i> Cari Penginapan</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-white py-5">
	<div class="container pt-3">
		<div class="row">
			<div class="col-12">
				<h4 class="mb-4 text-secondary text-center">Destinasi Populer</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-4">
				<div class="card  card-destination " style="background-image: url('../assets/images/destinasi/padang.jpg')">
					<a href="product.php" class="justify-content-center align-items-center text-center d-flex w-100 h-100 text-white flex-column">
						<div class="overlay"></div>
						<h4 class="card-title mb-1">Padang</h4>
						<p class="card-text"><strong>326 Penginapan</strong></p>
					</a>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="card  card-destination " style="background-image: url('../assets/images/destinasi/medan.jpg')">
					<a href="product.php" class="justify-content-center align-items-center text-center d-flex w-100 h-100 text-white flex-column">
						<div class="overlay"></div>
						<h4 class="card-title mb-1">Medan</h4>
						<p class="card-text"><strong>376 Penginapan</strong></p>
					</a>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="card  card-destination " style="background-image: url('../assets/images/destinasi/malang.jpg')">
					<a href="product.php" class="justify-content-center align-items-center text-center d-flex w-100 h-100 text-white flex-column">
						<div class="overlay"></div>
						<h4 class="card-title mb-1">Malang</h4>
						<p class="card-text"><strong>498 Penginapan</strong></p>
					</a>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="card  card-destination " style="background-image: url('../assets/images/destinasi/solo.jpg')">
					<a href="product.php" class="justify-content-center align-items-center text-center d-flex w-100 h-100 text-white flex-column">
						<div class="overlay"></div>
						<h4 class="card-title mb-1">Solo</h4>
						<p class="card-text"><strong>398 Penginapan</strong></p>
					</a>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="card  card-destination " style="background-image: url('../assets/images/destinasi/bali.jpg')">
					<a href="product.php" class="justify-content-center align-items-center text-center d-flex w-100 h-100 text-white flex-column">
						<div class="overlay"></div>
						<h4 class="card-title mb-1">Bali</h4>
						<p class="card-text"><strong>721 Penginapan</strong></p>
					</a>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="card  card-destination " style="background-image: url('../assets/images/destinasi/bogor.jpg')">
					<a href="product.php" class="justify-content-center align-items-center text-center d-flex w-100 h-100 text-white flex-column">
						<div class="overlay"></div>
						<h4 class="card-title mb-1">Bogor</h4>
						<p class="card-text"><strong>401 Penginapan</strong></p>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pt-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4 class="mb-4 text-secondary text-center">Penginapan Populer</h4>
			</div>
		</div>
		<div class="product-slider-container">
			<div>
				<div class="card card-product">
					<a href="product-detail.php">
						<div class="card-img-container">
							<img class="card-img" style="background-image:url('../assets/images/products/1.jpg');" alt="">
							<div class="card-img-overlay p-1">
								<button class="btn btn-default btn-wishlist"><i class="fa fa-heart-o"></i></button>
								<span class="label-badge">DISKON 15%</span>
							</div>

						</div>
						<div class="card-body">
							<a href="product-detail.php" class="product-title">Homestay Bokiko Panjang Jalan</a>
							<p class="product-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
								 class="fa fa-star-o"></i></p>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-institution  mx-1"></i>
								BUMNAG Bersama Mandeh Tarusan Jaya</a>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-map-marker mx-1"></i>
								Mandeh, Sumbar</a>
							<p class="product-price">Rp 200.000 <span class="product-discount">Rp 300.000</span></p>
						</div>
					</a>
				</div>
			</div>
			<div>
				<div class="card card-product">
					<a href="product-detail.php">
						<div class="card-img-container">
							<img class="card-img" style="background-image:url('../assets/images/products/2.jpg');" alt="">
							<div class="card-img-overlay p-1">
								<button class="btn btn-default btn-wishlist"><i class="fa fa-heart-o"></i></button>
								<span class="label-badge">DISKON 15%</span>
							</div>
						</div>
						<div class="card-body">
							<a href="product-detail.php" class="product-title">Homestay Bokiko Panjang Jalan</a>
							<p class="product-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
								 class="fa fa-star-o"></i></p>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-institution  mx-1"></i>
								BUMNAG Bersama Mandeh Tarusan Jaya</a>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-map-marker mx-1"></i>
								Mandeh, Sumbar</a>
							<p class="product-price">Rp 200.000 <span class="product-discount">Rp 300.000</span></p>
						</div>
					</a>
				</div>
			</div>
			<div>
				<div class="card card-product">
					<a href="product-detail.php">
						<div class="card-img-container">
							<img class="card-img" style="background-image:url('../assets/images/products/3.webp');" alt="">
							<div class="card-img-overlay p-1">
								<button class="btn btn-default btn-wishlist"><i class="fa fa-heart-o"></i></button>
								<span class="label-badge">DISKON 15%</span>
							</div>
						</div>
						<div class="card-body">
							<a href="product-detail.php" class="product-title">Homestay Bokiko Panjang Jalan</a>
							<p class="product-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
								 class="fa fa-star-o"></i></p>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-institution  mx-1"></i>
								BUMNAG Bersama Mandeh Tarusan Jaya</a>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-map-marker mx-1"></i>
								Mandeh, Sumbar</a>
							<p class="product-price">Rp 200.000 <span class="product-discount">Rp 300.000</span></p>
						</div>
					</a>
				</div>
			</div>
			<div>
				<div class="card card-product">
					<a href="product-detail.php">
						<div class="card-img-container">
							<img class="card-img" style="background-image:url('../assets/images/products/4.webp');" alt="">
							<div class="card-img-overlay p-1">
								<button class="btn btn-default btn-wishlist"><i class="fa fa-heart-o"></i></button>
								<span class="label-badge">DISKON 15%</span>
							</div>
						</div>
						<div class="card-body">
							<a href="product-detail.php" class="product-title">Homestay Bokiko Panjang Jalan</a>
							<p class="product-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
								 class="fa fa-star-o"></i></p>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-institution  mx-1"></i>
								BUMNAG Bersama Mandeh Tarusan Jaya</a>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-map-marker mx-1"></i>
								Mandeh, Sumbar</a>
							<p class="product-price">Rp 200.000 <span class="product-discount">Rp 300.000</span></p>
						</div>
					</a>
				</div>
			</div>
			<div>
				<div class="card card-product">
					<a href="product-detail.php">
						<div class="card-img-container">
							<img class="card-img" style="background-image:url('../assets/images/products/5.jpg');" alt="">
							<div class="card-img-overlay p-1">
								<button class="btn btn-default btn-wishlist"><i class="fa fa-heart-o"></i></button>
								<span class="label-badge">DISKON 15%</span>
							</div>

						</div>
						<div class="card-body">
							<a href="product-detail.php" class="product-title">Homestay Bokiko Panjang Jalan</a>
							<p class="product-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
								 class="fa fa-star-o"></i></p>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-institution  mx-1"></i>
								BUMNAG Bersama Mandeh Tarusan Jaya</a>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-map-marker mx-1"></i>
								Mandeh, Sumbar</a>
							<p class="product-price">Rp 200.000 <span class="product-discount">Rp 300.000</span></p>
						</div>
					</a>
				</div>
			</div>
			<div>
				<div class="card card-product">
					<a href="product-detail.php">
						<div class="card-img-container">
							<img class="card-img" style="background-image:url('../assets/images/products/6.webp');" alt="">
							<div class="card-img-overlay p-1">
								<button class="btn btn-default btn-wishlist"><i class="fa fa-heart-o"></i></button>
								<span class="label-badge">DISKON 15%</span>
							</div>
						</div>
						<div class="card-body">
							<a href="product-detail.php" class="product-title">Homestay Bokiko Panjang Jalan</a>
							<p class="product-rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
								 class="fa fa-star"></i>(12)</p>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-institution  mx-1"></i>
								BUMNAG Bersama Mandeh Tarusan Jaya</a>
							<a href="vendor-profile.php" class="product-vendor text-truncate"><i class="fa fa-fw fa-map-marker mx-1"></i>
								Mandeh, Sumbar</a>
							<p class="product-price">Rp 200.000 <span class="product-discount">Rp 300.000</span></p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-white py-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4 class="mb-4 text-secondary text-center">Mengapa DesaWisata?</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-6 text-center mb-4">
				<img class="mx-auto d-block my-4" style="height: 150px; width: auto;" src="../assets/images/bumidesa/1.png">
				<h5 class="text-secondary">Best Price Guarantee</h5>
				<p class="px-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic modi nihil enim ipsam sed explicabo
					ea magnam, eos reprehenderit molestias.</p>
			</div>
			<div class="col-12 col-md-6 text-center mb-4">
				<img class="mx-auto d-block my-4" style="height: 150px; width: auto;" src="../assets/images/bumidesa/2.png">
				<h5 class="text-secondary">Pasar Nasional dan Global</h5>
				<p class="px-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic modi nihil enim ipsam sed explicabo
					ea magnam, eos reprehenderit molestias.</p>
			</div>
			<div class="col-12 col-md-6 text-center mb-4">
				<img class="mx-auto d-block my-4" style="height: 150px; width: auto;" src="../assets/images/bumidesa/3.png">
				<h5 class="text-secondary">Diskon & Promo</h5>
				<p class="px-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic modi nihil enim ipsam sed explicabo
					ea magnam, eos reprehenderit molestias.</p>
			</div>
			<div class="col-12 col-md-6 text-center mb-4">
				<img class="mx-auto d-block my-4" style="height: 150px; width: auto;" src="../assets/images/bumidesa/4.png">
				<h5 class="text-secondary">Kemudahan Pembayaran</h5>
				<p class="px-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic modi nihil enim ipsam sed explicabo
					ea magnam, eos reprehenderit molestias.</p>
			</div>
		</div>
	</div>
</section>
