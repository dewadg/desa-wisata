<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="section-auth-bg py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">
                <div class="card my-5 p-4">
                    <h2 class="text-center">Masuk</h2>
                    <p class="text-center">Masuk ke akun anda</p>

                    <?php if (! is_null($this->session->flashdata('errors'))): ?>
                        <div class="text-left alert alert-danger alert-dismissible fade show" role="alert">
                            <?php if (is_array($this->session->flashdata('errors'))): ?>
                                <?php foreach ($this->session->flashdata('errors') as $field => $error): ?>
                                    <p><?= $error ?></p>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p><?= $this->session->flashdata('errors') ?></p>
                            <?php endif; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>

                    <form action="" method="post">
                        <?= generate_csrf_field() ?>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="text" name="email" class="form-control" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="row my-3">
                            <div class="col-6">
                                <a href="<?= base_url('auth/forgot') ?>">Lupa Kata Sandi?</a> | <a href="<?= base_url('register') ?>">Daftar</a>
                            </div>
                        </div>
                        <button class="btn btn-secondary btn-block" type="submit">Masuk</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
