<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<footer class="footer text-white pt-5 py-">
	<div class="container">
		<div class="row">
			<div class="col-md-3 mb-3">
				<h5 class="mb-4 text-regular">Hubungi Kami</h5>
				<div class="d-flex">
					<div class="pr-2 py-1">
						<i class="lnr lnr-envelope"></i>
					</div>
					<div class="pr-2 py-1">
						<p class="mb-0">dm@desawisata.com</p>
					</div>
				</div>
				<div class="d-flex">
					<div class="pr-2 py-1">
						<i class="lnr lnr-phone"></i>
					</div>
					<div class="pr-2 py-1">
						<p class="mb-0">+62855 1919191</p>
					</div>
				</div>
				<div class="d-flex">
					<div class="pr-2 py-1">
						<i class="lnr lnr-map-marker"></i>
					</div>
					<div class="pr-2 py-1">
						<p class="mb-0">Jl. Lengkong Gudang Tim. II <br> No.88, Lengkong Gudang Tim, <br>Serpong, Kota Tangerang Selatan</p>
					</div>
				</div>
			</div>
			<div class="col-md-3 mb-3">
				<h5 class="mb-4 text-regular">Tentang Kami</h5>
				<ul>
					<li><a href="<?= base_url('/apa-itu-desa-wisata') ?>">Apa itu DesaWisata?</a></li>
					<li><a href="<?= base_url('/keuntungan-bertransaksi') ?>">Keuntungan bertransaksi di DesaWisata</a></li>
					<li><a href="<?= base_url('/aliran-transaksi') ?>">Aliran Transaksi di DesaWisata</a></li>

				</ul>
			</div>
			<div class="col-md-3 mb-3">
				<h5 class="mb-4 text-regular">Tautan</h5>
				<ul>
					<li><a href="<?= base_url('/syarat-ketentuan') ?>">Syarat dan Ketentuan</a></li>
					<li><a href="<?= base_url('/kebijakan-privasi') ?>">Kebijakan Privasi</a></li>
					<li><a href="<?= base_url('/pembayaran') ?>">Tata Cara Pembayaran</a></li>
					<li><a href="<?= base_url('/keamanan-bertransaksi') ?>">Keamanan Bertransaksi</a></li>
				</ul>
			</div>
			<div class="col-md-3 mb-3">
				<h5 class="mb-4 text-regular">Metode Pembayaran</h5>
				<img src="<?= base_url('/assets/images/payment_method.png') ?>" alt="" class="">
			</div>
		</div>
		<div class="row copy py-4 mt-5">
			<div class="col">
				<p class="text-center mb-0">&copy; <?= date('Y') ?> PT. Sembilan Satu Investama</p>
			</div>
		</div>
	</div>
</footer>

<script src="<?= base_url('/assets/js/local/slider.js') ?>"></script>
<script src="<?= base_url('/assets/js/local/global.js') ?>"></script>
<?php if (isLandingPage()): ?>
	<script src="<?= base_url('/assets/js/local/hotel-form.js') ?>"></script>
<?php endif; ?>
</body>
</html>
