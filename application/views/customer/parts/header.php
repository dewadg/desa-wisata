<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title><?= $this->config->item('site_title') . ': ' . $this->config->item('site_tagline') ?></title>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="<?= $this->config->item('site_description') ?>" name="description">
    <meta content="<?= $this->config->item('site_keywords') ?>" name="keywords">
	<link href="<?= base_url('/assets/css/style.css') ?>" rel="stylesheet" type="text/css">
	<script src="<?= base_url('/assets/js/vendor.js') ?>"></script>
	<!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script> -->
	<style>
		.select2-dropdown--below {
			top: -38px;
		}

		.select-box {
			width: 300px;
		}

		.select-box {
			position: relatove;
			display: block;
		}

		.select2-container--default .select2-selection--single {
			background-color: #fff;
			border: 0;
			border-radius: 4px;
			height: 36px;
			line-height: 36px;
		}

		.select2-container--default .select2-selection--single .select2-selection__rendered {
			line-height: 34px;
		}

		.select2-container--default .select2-selection--single .select2-selection__arrow {
			top: 5px;
		}

		.select2-container {
			height: 38px;
			border: solid 1px #ddd;
		}

	</style>
</head>

<body>
	<div class="topbar">
		<div class="container clearfix">
			<div class="topbar-tagline d-none d-sm-block">
				<p>Temukan Keindahan Tersembunyi Indonesia</p>
			</div>
			<div class="topbar-menu">
				<ul>
					<li class="d-none d-md-block">
						<a class="new-tab-link" href="<?= base_url('/daftar') ?>">Pendaftaran BUMDes</a>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Mengenai DesaWisata</a>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item new-tab-link" href="<?= base_url('/apa-itu-desa-wisata') ?>">Apa itu DesaWisata</a>
							<div class="d-block d-md-none">
								<div class="dropdown-divider"></div>
								<a class="dropdown-item new-tab-link" href="<?= base_url('/daftar') ?>">Pendaftaran BUMDes</a>
								<div class="dropdown-divider"></div>
							</div>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/keuntungan-bertransaksi') ?>">Keuntungan Bertransaksi di DesaWisata</a>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/cara-menyewakan') ?>">Cara Menyewakan di DesaWisata</a>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/cara-memesan') ?>">Cara Memesan di DesaWisata</a>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/pembayaran') ?>">Pembayaran di DesaWisata</a>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/aliran-transaksi') ?>">Aliran Transaksi di DesaWisata</a>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/keamanan-bertransaksi') ?>">Keamanan Bertransaksi di DesaWisata</a>
						</div>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">Informasi</a>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item new-tab-link" href="<?= base_url('/syarat-ketentuan') ?>">Syarat dan Ketentuan</a>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/kebijakan-privasi') ?>">Kebijakan Privasi</a>
							<a class="dropdown-item new-tab-link" href="<?= base_url('/bantuan') ?>">Bantuan</a>
							<a class="dropdown-item new-tab-link d-md-none" href="<?= base_url('/hubungi-kami') ?>">Hubungi Kami</a>
						</div>
					</li>
					<li class="d-none d-md-block">
						<a class="new-tab-link" href="<?= base_url('/hubungi-kami') ?>">Hubungi Kami</a>
					</li>
					<li>
						<a class="dropdown-toggle" data-toggle="dropdown" href="#"></a>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item active" href="#">Bahasa Indonesia</a>
							<a class="dropdown-item" href="#">English</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light bg-white">
		<div class="container">
			<a class="navbar-brand" href="<?= base_url() ?>"><img alt="DesaWisata" src="<?= base_url('/assets/images/logo.png') ?>"></a>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<?php if (isLogged()): ?>
                        <li class="nav-item nav-icon dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdown">
                                <i class="lnr lnr-user"></i>
                            </a>
                            <div aria-labelledby="navbarDropdown" class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Profil</a>
                                <a class="dropdown-item" href="#">Transaksi</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= base_url('logout') ?>">Keluar</a>
                            </div>
                        </li>
					<?php else: ?>
                        <li class="nav-item dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle no-caret" href="<?= base_url('login') ?>">Masuk</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle no-caret" href="<?= base_url('register') ?>">Daftar</a>
                        </li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</nav>
