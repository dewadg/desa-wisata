<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="section-red-bg py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 text-white text-center">
				<h1>Pembayaran</h1>
				<h3>DesaWisata memberikan pilihan pembayaran yang beragam bekerja sama dengan mitra-mitra pembayaran yang Aman dan
					Terpercaya.</h3>
			</div>
		</div>
	</div>
</section>

<div class="py-5 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-7 align-self-center">
				<h3 class="text-secondary">Finpay (Online)</h3>
				<p class="my-3">DesaWisata telah bekerja sama dengan Finnet sebagai salah satu cara bayar yang cukup fleksibel, mudah
					dan nyaman.</p>
				<p class="my-3">Pembayaran menggunakan Finpay semudah membayar tagihan PSTN (Telepon) Anda.</p>
				<p class="my-3">Cukup lakukan pembayaran seperti halnya membayar tagihan telepon, dengan mengganti nomor telepon
					dengan Kode Bayar yang kami berikan.</p>
			</div>
			<div class="col-md-5">
				<img class="img-fluid d-block" src="https://www.bumidesa.com/assets/images/pembayaran/finpay.jpg"> </div>
		</div>
	</div>
</div>
<div class="py-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-5 order-2 order-md-1 d-flex align-items-center">
        <img class="img-fluid d-block" src="https://www.bumidesa.com/assets/images/pembayaran/permata.png">
      </div>
			<div class="col-md-7 order-1 order-md-2">
				<h3 class="text-secondary">Virtual Account (Online)</h3>
				<p class="my-3">Dengan menggunakan Virtual Account, Anda dapat melakukan pembayaran di ATM Bank manapun di
					Indonesia yang sudah terhubung dengan jaringan ATM Bersama, PRIMA dan ALTO.</p>
				<p class="my-3">Anda juga dapat lakukan transfer online dari Internet Banking dan Mobile Banking Anda (jika Bank
					Anda sudah menyediakannya).</p>
				<p class="my-3">Cukup dengan melakukan transfer ke rekening Bank Permata dari rekening Bank Anda. Gunakan Kode
					Bayar Virtual Account (16 digit) sebagai nomor rekening tujuan.</p>
				<p class="my-3">Bagi nasabah Bank Permata, Anda dapat gunakan menu "Pembayaran Virtual Account" dari semua channel
					Bank Permata.</p>
			</div>
		</div>
	</div>
</div>
<div class="py-5 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-7 align-self-center">
				<h3 class="text-secondary">Mini Market (Online)</h3>
				<p class="my-3">Mini market yang saat ini sudah tersebar luas hingga ke berbagai pelosok daerah di Indonesia
					tentunya menjadi pilihan yang nyaman untuk melakukan pembayaran.</p>
				<p class="my-3">Anda juga dapat melakukan pembayaran di Mini Market yang sudah bekerja sama dengan kami. (Alfa
					Mart)</p>
			</div>
			<div class="col-md-5 d-flex align-items-center">
				<img class="img-fluid d-block mx-auto" src="https://www.bumidesa.com/assets/images/pembayaran/ALFAMART_LOGO_BARU.png">
			</div>
		</div>
	</div>
</div>
<div class="py-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-5 order-2 order-md-1 ">
				<img class="img-fluid d-block mx-auto" src="https://www.bumidesa.com/assets/images/pembayaran/visa-mastercard-secure.jpg">
				<img class="img-fluid d-block mx-auto" src="https://www.bumidesa.com/assets/images/pembayaran/pci_logo.png"> </div>
			<div class="col-md-7 order-1 order-md-2">
				<h3 class="text-secondary">Kartu Kredit (Online)</h3>
				<p class="my-3">Pembayaran dengan Kartu Kredit sangat umum dilakukan dalam transaksi lintas negara (Internasional),
					dikarenakan jaringannya yang sangat luas.</p>
				<p class="my-3">Untuk kenyamanan, DesaWisata juga menyediakan pilihan pembayaran dengan menggunakan Kartu Kredit,
					khususnya bagi Anda yang memiliki Kartu Kredit dengan logo Visa atau MasterCard.</p>
				<p class="my-3">Kami sudah mengadopsi keamanan transaksi sesuai standar pembayaran internasional (PCI-DSS) dan juga
					3DS (3D Secure), yang akan meminimalkan terjadinya fraud.</p>
			</div>
		</div>
	</div>
</div>
<div class="py-5 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-7 align-self-center">
				<h3 class="text-secondary">Bank Transfer (Offline)</h3>
				<p class="my-3">Bagi Anda yang lebih nyaman menggunakan fasilitas Transfer antar Rekening (Bank Transfer), kami
					juga menyediakan fasilitas pembayaran yang sudah umum ini.</p>
				<p class="my-3">Cukup transfer pembayaran Anda ke rekening kami di Bank Mandiri dan setelah itu lakukan konfirmasi
					pembayaran sekaligus dengan melampirkan bukti pembayaran Anda.</p>
				<p class="my-3">Berikut ini data rekening kami:</p>
				<ul class="">
					<li>Nama Rekening : PT. Sembilan Satu Investama</li>
					<li>Nomor Rekening:&nbsp;164 0099 9191 95</li>
				</ul>
			</div>
			<div class="col-md-5 d-flex align-items-center">
				<img class="img-fluid d-block mx-auto" src="https://www.bumidesa.com/assets/images/pembayaran/layout_set_logo.png">
			</div>
		</div>
	</div>
</div>
