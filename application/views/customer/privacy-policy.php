<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="section-red-bg py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 text-white text-center">
				<h1>Kebijakan Privasi</h1>
			</div>
		</div>
	</div>
</section>

<div class="py-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="">Adanya Kebijakan Privasi ini adalah komitmen nyata dari
					<strong>DesaWisata</strong> untuk menghargai dan melindungi setiap data atau informasi pribadi Pengguna situs
					<strong>www.desawisata.com</strong>, situs-situs turunannya, serta aplikasi gawai
					<strong>DesaWisata</strong> (selanjutnya disebut sebagai "Situs").</p>
				<p class="">Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs
					<strong>DesaWisata</strong> sebagaimana tercantum dalam "Syarat &amp; Ketentuan" dan informasi lain yang tercantum
					di Situs) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pembukaan,
					dan/atau segala bentuk pengelolaan
					yang terkait dengan data atau informasi yang Pengguna berikan kepada
					<strong>DesaWisata</strong> atau yang
					<strong>DesaWisata</strong> kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna
					melakukan pendaftaran di Situs, mengakses Situs, maupun mempergunakan layanan-layanan pada Situs (selanjutnya
					disebut sebagai "data").</p>
				<p class="">Dengan mengakses dan/atau mempergunakan layanan
					<strong>DesaWisata</strong>, Pengguna menyatakan bahwa setiap data Pengguna merupakan data yang benar dan sah, serta
					Pengguna memberikan persetujuan kepada
					<strong>DesaWisata</strong> untuk memperoleh, mengumpulkan, menyimpan, mengelola dan mempergunakan data tersebut
					sebagaimana tercantum dalam Kebijakan Privasi maupun Syarat dan Ketentuan
					<strong>DesaWisata</strong>.</p>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-secondary">Perolehan dan Pengumpulan Data Pengguna</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="lead">
					<strong>DesaWisata</strong> mengumpulkan data Pengguna dengan tujuan untuk memproses transaksi Pengguna, mengelola
					dan memperlancar proses penggunaan Situs, serta tujuan-tujuan lainnya selama diizinkan oleh peraturan
					perundang-undangan yang berlaku.
					Adapun data Pengguna yang dikumpulkan adalah sebagai berikut:</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>Data yang diserahkan secara mandiri oleh Pengguna, termasuk namun tidak terbatas pada data yang diserahkan
						pada saat Pengguna:</li>
					<ul>
						<li>Membuat atau memperbarui akun
							<strong>DesaWisata</strong>, termasuk diantaranya nama pengguna (username), alamat email, nomor telepon, password,
							alamat, foto, dan lain-lain;</li>
						<li>Menghubungi
							<strong>DesaWisata</strong>, termasuk melalui layanan konsumen;</li>
						<li>Mengisi survei yang dikirimkan oleh
							<strong>DesaWisata</strong> atau atas nama
							<strong>DesaWisata</strong>;</li>
						<li>Melakukan interaksi dengan Pengguna lainnya melalui fitur pesan, diskusi produk, ulasan, rating, Pusat
							Bantuan dan sebagainya;</li>
						<li>Mempergunakan layanan-layanan pada Situs, termasuk data transaksi yang detil, diantaranya jenis, jumlah
							dan/atau keterangan dari produk atau layanan yang dibeli, alamat pengiriman, kanal pembayaran yang digunakan,
							jumlah transaksi, tanggal
							dan waktu transaksi, serta detil transaksi lainnya;</li>
						<li>Mengisi data-data pembayaran pada saat Pengguna melakukan aktivitas transaksi pembayaran melalui Situs,
							termasuk namun tidak terbatas pada data rekening bank, kartu kredit, virtual account, instant payment, internet
							banking, gerai ritel;
							dan/atau</li>
						<li>Menggunakan fitur yang membutuhkan izin akses terhadap perangkat Pengguna.</li>
					</ul>
					<li>Data yang terekam pada saat Pengguna mempergunakan Situs, termasuk namun tidak terbatas pada:</li>
					<ul>
						<li>Data lokasi riil atau perkiraannya seperti alamat IP, lokasi Wi-Fi, geo-location, dan sebagainya;</li>
						<li>Data berupa waktu dari setiap aktivitas Pengguna, termasuk aktivitas pendaftaran, login, transaksi, dan lain
							sebagainya;</li>
						<li>Data penggunaan atau preferensi Pengguna, diantaranya interaksi Pengguna dalam menggunakan Situs, pilihan
							yang disimpan, serta pengaturan yang dipilih. Data tersebut diperoleh menggunakan cookies, pixel tags, dan
							teknologi serupa yang menciptakan
							dan mempertahankan pengenal unik;</li>
						<li>Data perangkat, diantaranya jenis perangkat yang digunakan untuk mengakses Situs, termasuk model perangkat
							keras, sistem operasi dan versinya, perangkat lunak, nama file dan versinya, pilihan bahasa, pengenal perangkat
							unik, pengenal iklan,
							nomor seri, informasi gerakan perangkat, dan/atau informasi jaringan seluler;</li>
						<li>Data catatan (log), diantaranya catatan pada server yang menerima data seperti alamat IP perangkat, tanggal
							dan waktu akses, fitur aplikasi atau laman yang dilihat, proses kerja aplikasi dan aktivitas sistem lainnya,
							jenis peramban, dan/atau
							situs atau layanan pihak ketiga yang Anda gunakan sebelum berinteraksi dengan Situs.</li>
					</ul>
					<li>Data yang diperoleh dari sumber lain, termasuk:</li>
					<ul>
						<li>Mitra usaha
							<strong>DesaWisata</strong> yang turut membantu
							<strong>DesaWisata</strong> dalam mengembangkan dan menyajikan layanan-layanan dalam Situs kepada Pengguna, antara
							lain mitra penyedia layanan pembayaran, logistik atau kurir, infrastruktur situs, dan mitra-mitra lainnya.</li>
						<li>Mitra usaha
							<strong>DesaWisata</strong> tempat Pengguna membuat atau mengakses akun
							<strong>DesaWisata</strong>, seperti layanan media sosial, atau situs/aplikasi yang menggunakan API
							<strong>DesaWisata</strong> atau yang digunakan
							<strong>DesaWisata</strong>;</li>
						<li>Penyedia layanan finansial;</li>
						<li>Penyedia layanan pemasaran;</li>
						<li>Sumber yang tersedia secara umum.</li>
					</ul>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="">
					<strong>DesaWisata</strong> dapat menggabungkan data yang diperoleh dari sumber tersebut dengan data lain yang
					dimilikinya.</p>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-secondary">Penggunaan Data</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="lead">
					<strong>DesaWisata</strong> dapat menggunakan keseluruhan atau sebagian data yang diperoleh dan dikumpulkan dari
					Pengguna sebagaimana disebutkan dalam bagian sebelumnya untuk hal-hal sebagai berikut:</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>Memproses segala bentuk permintaan, aktivitas maupun transaksi yang dilakukan oleh Pengguna melalui Situs,
						termasuk untuk keperluan pengiriman produk kepada Pengguna.</li>
					<li>Penyediaan fitur-fitur untuk memberikan, mewujudkan, memelihara dan memperbaiki produk dan layanan kami,
						termasuk:</li>
					<ul>
						<li>Menawarkan, memperoleh, menyediakan, atau memfasilitasi layanan marketplace, asuransi, pembiayaan, pinjaman,
							maupun produk-produk lainnya melalui Situs;</li>
						<li>Memungkinkan fitur untuk mempribadikan akun Pengguna
							<strong>DesaWisata</strong>, seperti Wishlist dan Pemasok Favorit; dan/atau</li>
						<li>Melakukan kegiatan internal yang diperlukan untuk menyediakan layanan pada situs/aplikasi
							<strong>DesaWisata</strong>, seperti pemecahan masalah perangkat lunak, bug, permasalahan operasional, melakukan
							analisis data, pengujian, dan penelitian, dan untuk memantau dan menganalisis kecenderungan penggunaan dan
							aktivitas.</li>
					</ul>
					<li>Membantu Pengguna pada saat berkomunikasi dengan Layanan Pelanggan
						<strong>DesaWisata</strong>, diantaranya untuk:</li>
					<ul>
						<li>Memeriksa dan mengatasi permasalahan Pengguna;</li>
						<li>Mengarahkan pertanyaan Pengguna kepada petugas Layanan Pelanggan yang tepat untuk mengatasi permasalahan; dan</li>
						<li>Mengawasi dan memperbaiki tanggapan Layanan Pelanggan
							<strong>DesaWisata</strong>.</li>
					</ul>
					<li>Menghubungi Pengguna melalui email, surat, telepon, fax, dan lain-lain, termasuk namun tidak terbatas, untuk
						membantu dan/atau menyelesaikan proses transaksi maupun proses penyelesaian kendala.</li>
					<li>Menggunakan informasi yang diperoleh dari Pengguna untuk tujuan penelitian, analisis, pengembangan dan
						pengujian produk guna meningkatkan keamanan dan keamanan layanan-layanan pada Situs, serta mengembangkan fitur
						dan produk baru.</li>
					<li>Menginformasikan kepada Pengguna terkait produk, layanan, promosi, studi, survei, berita, perkembangan
						terbaru, acara dan lain-lain, baik melalui Situs maupun melalui media lainnya.
						<strong>DesaWisata</strong> juga dapat menggunakan informasi tersebut untuk mempromosikan dan memproses kontes dan
						undian, memberikan hadiah, serta menyajikan iklan dan konten yang relevan tentang layanan
						<strong>DesaWisata</strong> dan mitra usahanya.</li>
					<li>Melakukan monitoring ataupun investigasi terhadap transaksi-transaksi mencurigakan atau transaksi yang
						terindikasi mengandung unsur kecurangan atau pelanggaran terhadap Syarat dan Ketentuan atau ketentuan hukum yang
						berlaku, serta melakukan
						tindakan-tindakan yang diperlukan sebagai tindak lanjut dari hasil monitoring atau investigasi transaksi
						tersebut.</li>
					<li>Dalam keadaan tertentu,
						<strong>DesaWisata</strong> mungkin perlu untuk menggunakan ataupun mengungkapkan data Pengguna untuk tujuan
						penegakan hukum atau untuk pemenuhan persyaratan hukum dan peraturan yang berlaku, termasuk dalam hal terjadinya
						sengketa atau proses hukum
						antara Pengguna dan
						<strong>DesaWisata</strong>.</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-secondary">Pengungkapan Data Pribadi Pengguna</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="lead">
					<strong>DesaWisata</strong> menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan data pribadi Anda
					kepada pihak ketiga lain, tanpa terdapat izin dari Anda, kecuali dalam hal-hal sebagai berikut:</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>Dibutuhkan adanya pengungkapan data Pengguna kepada mitra atau pihak ketiga lain yang membantu
						<strong>DesaWisata</strong> dalam menyajikan layanan pada Situs dan memproses segala bentuk aktivitas Pengguna
						dalam Situs, termasuk memproses transaksi, verifikasi pembayaran, pengiriman produk, dan lain-lain.</li>
					<li>
						<strong>DesaWisata</strong> dapat menyediakan informasi yang relevan kepada mitra usaha sesuai dengan persetujuan
						Pengguna untuk menggunakan layanan mitra usaha, termasuk diantaranya aplikasi atau situs lain yang telah saling
						mengintegrasikan API
						atau layanannya, atau mitra usaha yang mana
						<strong>DesaWisata</strong> telah bekerjasama untuk mengantarkan promosi, kontes, atau layanan yang dikhususkan</li>
					<li>Dibutuhkan adanya komunikasi antara mitra usaha
						<strong>DesaWisata</strong> (seperti penyedia logistik, pembayaran, dan lain-lain) dengan Pengguna dalam hal
						penyelesaian kendala maupun hal-hal lainnya.</li>
					<li>
						<strong>DesaWisata</strong> dapat menyediakan informasi yang relevan kepada vendor, konsultan, mitra pemasaran,
						firma riset, atau penyedia layanan sejenis.</li>
					<li>Pengguna menghubungi
						<strong>DesaWisata</strong> melalui media publik seperti blog, media sosial, dan fitur tertentu pada Situs,
						komunikasi antara Pengguna dan
						<strong>DesaWisata</strong> mungkin dapat dilihat secara publik.</li>
					<li>
						<strong>DesaWisata</strong> dapat membagikan informasi Pengguna kepada anak perusahaan dan afiliasinya untuk
						membantu memberikan layanan atau melakukan pengolahan data untuk dan atas nama
						<strong>DesaWisata</strong>.</li>
					<li>
						<strong>DesaWisata</strong> mengungkapkan data Pengguna dalam upaya mematuhi kewajiban hukum dan/atau adanya
						permintaan yang sah dari aparat penegak hukum.</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-secondary">Cookies</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat Pengguna yang
						menjalankan fungsi dalam menyimpan preferensi maupun konfigurasi Pengguna selama mengunjungi suatu situs.</li>
					<li>Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses data lain yang Pengguna miliki
						di perangkat komputer Pengguna, selain dari yang telah Pengguna setujui untuk disampaikan.</li>
					<li>Walaupun secara otomatis perangkat komputer Pengguna akan menerima cookies, Pengguna dapat menentukan pilihan
						untuk melakukan modifikasi melalui pengaturan browser Pengguna yaitu dengan memilih untuk menolak cookies
						(pilihan ini dapat membatasi
						layanan optimal pada saat melakukan akses ke Situs).</li>
					<li>
						<strong>DesaWisata</strong> menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh
						dari fitur tersebut, seperti umur, jenis kelamin, minat Pengguna dan lain-lain, akan kami gunakan untuk
						pengembangan Situs dan konten
						<strong>DesaWisata</strong>. Jika tidak ingin data Anda terlacak oleh Google Analytics, Anda dapat menggunakan
						Add-On Google Analytics Opt-Out Browser.</li>
					<li>
						<strong>DesaWisata</strong> dapat menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka
						meningkatkan layanan dan konten
						<strong>DesaWisata</strong>, termasuk diantaranya ialah penyesuaian dan penyajian iklan kepada setiap Pengguna
						berdasarkan minat atau riwayat kunjungan. Jika Anda tidak ingin iklan ditampilkan berdasarkan penyesuaian
						tersebut, maka Anda dapat mengaturnya
						melalui browser.</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-secondary">Pilihan Pengguna dan Transparansi</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ol>
					<li>Perangkat seluler pada umumnya (iOS, Android, dan sebagainya) memiliki pengaturan sehingga aplikasi
						<strong>DesaWisata</strong> tidak dapat mengakses data tertentu tanpa persetujuan dari Pengguna. Perangkat iOS akan
						memberikan pemberitahuan kepada Pengguna saat aplikasi
						<strong>DesaWisata</strong> pertama kali meminta akses terhadap data tersebut, sedangkan perangkat Android akan
						memberikan pemberitahuan kepada Pengguna saat aplikasi
						<strong>DesaWisata</strong> pertama kali dimuat. Dengan menggunakan aplikasi dan memberikan akses terhadap
						aplikasi, Pengguna dianggap memberikan persetujuannya terhadap Kebijakan Privasi.</li>
					<li>Setelah transaksi jual beli melalui marketplace berhasil, Penjual maupun Pembeli memiliki kesempatan untuk
						memberikan penilaian dan ulasan terhadap satu sama lain. Informasi ini mungkin dapat dilihat secara publik dengan
						persetujuan Pengguna.</li>
					<li>Pengguna dapat mengakses dan mengubah informasi berupa alamat email, nomor telepon, tanggal lahir, jenis
						kelamin, daftar alamat, metode pembayaran, dan rekening bank melalui fitur Pengaturan pada Situs.</li>
					<li>Pengguna dapat menarik diri dari informasi atau notifikasi berupa buletin, ulasan, diskusi produk, pesan
						pribadi, atau pesan pribadi dari Admin yang dikirimkan oleh
						<strong>DesaWisata</strong> melalui fitur Pengaturan pada Situs.
						<strong>DesaWisata</strong> tetap dapat mengirimkan pesan atau email berupa keterangan transaksi atau informasi
						terkait akun Pengguna.</li>
					<li>Sejauh diizinkan oleh ketentuan yang berlaku, Pengguna dapat menghubungi
						<strong>DesaWisata</strong> untuk melakukan penarikan persetujuan terhadap perolehan, pengumpulan, penyimpanan,
						pengelolaan dan penggunaan data Pengguna. Apabila terjadi demikian maka Pengguna memahami konsekuensi bahwa
						Pengguna tidak dapat menggunakan
						layanan Situs maupun layanan
						<strong>DesaWisata</strong> lainnya.</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-secondary">Penyimpanan dan Penghapusan Informasi</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="lead">
					<strong>DesaWisata</strong> akan menyimpan informasi selama akun Pengguna tetap aktif dan dapat melakukan
					penghapusan sesuai dengan ketentuan peraturan hukum yang berlaku.</p>
			</div>
		</div>
	</div>
</div>
<div class="pb-5 bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="text-secondary">Pembaruan Kebijakan Privasi</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p class="lead">
					<strong>DesaWisata</strong> dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap Kebijakan Privasi ini.
					<strong>DesaWisata</strong> menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi
					ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Situs
					maupun layanan
					<strong>DesaWisata</strong> lainnya, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi.</p>
			</div>
		</div>
	</div>
</div>
