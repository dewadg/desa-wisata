<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="section-auth-bg py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <div class="card my-5 p-4">
                    <h2 class="text-center">Daftar</h2>
                    <p class="text-center">Lengkapi formulir registrasi berikut</p>
                    <?php if (! is_null($this->session->flashdata('errors'))): ?>
                        <div class="text-left alert alert-danger alert-dismissible fade show" role="alert">
                            <?php if (is_array($this->session->flashdata('errors'))): ?>
                                <?php foreach ($this->session->flashdata('errors') as $field => $error): ?>
                                    <p><?= $error ?></p>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <p><?= $this->session->flashdata('errors') ?></p>
                            <?php endif; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url('register') ?>" method="post">
                        <?= generate_csrf_field() ?>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
                                </div>
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="text" name="email" class="form-control" placeholder="E-mail">
                                    <small class="form-text text-muted">Email ini akan digunakan sebagai akun untuk login serta untuk mengirimkan kode aktivasi dan informasi transaksi Anda.</small>
                                </div>
                                <div class="form-group">
                                    <label>No. Handphone</label>
                                    <input type="text" name="phone" class="form-control" placeholder="No. Handphone">
                                    <small class="form-text text-muted">Nomor handphone digunakan untuk menghubungi Anda bila diperlukan terkait dengan transaksi Anda.</small>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label>Ulangi Password</label>
                                    <input type="password" name="password_conf" class="form-control" placeholder="Ulangi Password">
                                    <small class="form-text text-muted">Pastikan password Anda cukup Aman untuk melindungi keamanan data Anda.</small>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <select name="provinceId" class="form-control">
                                        <option value="" disabled selected>Pilih Provinsi</option>
                                        <?php foreach ($provinces as $province): ?>
                                            <option value="<?= $province->id ?>">
                                                <?= $province->name ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kota / Kabupaten</label>
                                    <select name="cityId" class="form-control" disabled>
                                        <option value="" disabled selected>Pilih Kota / Kabupaten</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Kode Pos</label>
                                    <input type="text" name="postalCode" class="form-control" placeholder="Kode Pos">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="address" rows="3" class="form-control" placeholder="Alamat"></textarea>
                                    <small class="form-text text-muted">Pastikan dilengkapi dengan benar, karena alamat ini akan digunakan untuk surat-menyurat bila diperlukan.</small>
                                </div>
                                <div class="form-group">
                                    <label>Kode Referensi</label>
                                    <input type="text" name="referenceCode" class="form-control" placeholder="Kode Referensi">
                                    <small class="form-text text-muted">Silakan masukkan kode referensi dari rekan Anda dan raih kesempatan untuk mendapatkan free voucher belanja.</small>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button class="btn btn-secondary w-100" type="submit" style="max-width: 300px;">Daftar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('[name=provinceId]').on('change', function(){
            var id      = $('option:selected',this).val();
            var target  = $('[name=cityId]');

            target.prop('disabled',true);

            $.ajax({
                url: '<?= base_url() ?>ajax/forward/<?= $this->config->item('api_context_common') ?>/service/city/list',
                type: 'POST',
                data: {
                    provinceId: id,
                    <?= csrf_token('name') ?>: '<?= csrf_token() ?>'
                },
                success: function (result) {
                    result = result.list;

                    var option = '<option value="" disabled selected="">Pilih Kota/Kabupaten</option>';

                    for (var i = 0; i < result.length; i++) {
                        option += '<option  value="' + result[i].id + '">' + result[i].name + '</option>';
                    }

                    target.html(option).val(null);
                    target.prop('disabled', false);
                    target.trigger('chosen:updated');
                }
            });
        });
    });
</script>
