<section class="section-auth-bg py-5">
    <div class="container">
        <div class="row justify-content-center">
            <?php if ($this->config->item('email_activation')): ?>
                <div class="col-12 col-md-8">
                    <div class="stage-container">
                        <div class="stage col-md-4 col-sm-4 tmm-current ">
                            <div class="stage-header"><i class="fa fa-file"></i></div>
                            <div class="stage-content">
                                <h4 class="stage-title">Isi Formulir</h4>
                            </div>
                        </div>
                        <!--/ .stage-->

                        <div class="stage col-md-4 col-sm-4 tmm-current">
                            <div class="stage-header"><i class="fa fa-envelope"></i></div>

                            <div class="stage-content">
                                <h4 class="stage-title">Aktivasi Email</h4>
                            </div>
                        </div>
                        <!--/ .stage-->

                        <div class="stage col-md-4 col-sm-4 tmm-current">
                            <div class="stage-header"><i class="fa fa-check"></i></div>
                            <div class="stage-content">
                                <h4 class="stage-title">Selesai</h4>
                            </div>
                        </div>
                        <!--/ .stage-->
                    </div>
                </div>
            <?php endif; ?>

            <div class="col-12 col-md-5">
                <div class="card mb-5 mt-3 p-4 text-center">
                    <h1 class="text-secondary"><i class="fa fa-check"></i></h1>
                    <h2>Pendaftaran berhasil</h2>
                    <p>Akun anda sudah terdaftar, silahkan masuk dengan menekan tombol di bawah ini</p>
                    <a href="<?= base_url('login') ?>" class="btn btn-secondary">MASUK</a>
                </div>
            </div>
        </div>
    </div>
</section>
