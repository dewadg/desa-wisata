<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="section-red-bg py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 text-white text-center">
				<h1>Syarat dan Ketentuan</h1>
			</div>
		</div>
	</div>
</section>
<section class="bg-white">
	<div class="py-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="lead">Selamat datang di
						<strong>www.desawisata.com</strong>.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="">Syarat &amp; ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh
						<strong>PT. Sembilan Satu Investama</strong> terkait penggunaan situs
						<strong>www.desawisata.com</strong>. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak
						dan kewajiban Pengguna di bawah hukum.</p>
					<p class="">Dengan mendaftar dan/atau menggunakan situs
						<strong>www.desawisata.com</strong>, maka pengguna dianggap telah membaca, mengerti, memahami dan menyutujui semua
						isi dalam Syarat &amp; ketentuan. Syarat &amp; ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam
						sebuah perjanjian yang
						sah antara
						<strong>Pengguna</strong> dengan
						<strong>PT. Sembilan Satu Investama</strong>. Jika pengguna tidak menyetujui salah satu, sebagian, atau seluruh
						isi Syarat &amp; Ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di
						<strong>www.desawisata.com</strong>.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="py-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>A. DEFINISI</h3>
					<ol>
						<li>
							<strong>PT. Sembilan Satu Investama</strong> adalah suatu perseroan terbatas yang menjalankan kegiatan usaha
							jasa web portal
							<strong>www.desawisata.com</strong>, yakni situs pencarian barang yang dijual oleh penjual terdaftar, yang secara
							khusus merupakan
							<strong>Badan Usaha Milik Desa (BUMDes)</strong>. Selanjutnya disebut
							<strong>DesaWisata</strong>.</li>
						<li>Situs
							<strong>DesaWisata</strong> adalah
							<strong>www.desawisata.com</strong>.</li>
						<li>Syarat dan Ketentuan adalah perjanjian antara Pengguna dan
							<strong>DesaWisata</strong> yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab
							pengguna dan
							<strong>DesaWisata</strong>, serta tata cara penggunaan sistem layanan
							<strong>DesaWisata</strong>.</li>
						<li>Pengguna adalah pihak yang menggunakan layanan
							<strong>DesaWisata</strong>, termasuk namun tidak terbatas pada pembeli, penjual ataupun pihak lain yang sekedar
							berkunjung ke Situs
							<strong>DesaWisata</strong>.</li>
						<li>Pembeli adalah Pengguna terdaftar yang melakukan permintaan atas Barang yang dijual oleh Penjual di Situs
							<strong>DesaWisata</strong>.</li>
						<li>Penjual adalah Pengguna terdaftar yang melakukan penawaran atas suatu Barang kepada para Pengguna Situs
							<strong>DesaWisata</strong>.</li>
						<li>Barang adalah benda yang berwujud / memiliki fisik Barang yang dapat diantar / memenuhi kriteria pengiriman
							oleh perusahaan jasa pengiriman Barang.</li>
						<li>Rekening Resmi
							<strong>DesaWisata</strong> adalah rekening bersama yang disepakati oleh
							<strong>DesaWisata</strong> dan para pengguna untuk proses transaksi jual beli di Situs
							<strong>DesaWisata</strong>. Rekening resmi
							<strong>DesaWisata</strong> dapat ditemukan di halaman
							<span style="font-weight: bold;">Pembayaran di DesaWisata</span>.</li>
					</ol>
					<h3>B. AKUN, SALDO
						<strong>DesaWisata</strong>, PASSWORD DAN KEAMANAN</h3>
					<ol>
						<li>Pengguna dengan ini menyatakan bahwa pengguna adalah orang yang cakap dan mampu untuk mengikatkan dirinya
							dalam sebuah perjanjian yang sah menurut hukum.</li>
						<li>
							<strong>DesaWisata</strong> tidak memungut biaya pendaftaran kepada Pengguna.</li>
						<li>Pengguna yang telah mendaftar berhak bertindak sebagai:
							<br> - Pembeli
							<br> - Penjual, dengan melengkapi persyaratan yang diberikan.</li>
						<li>Pengguna yang akan bertindak sebagai Penjual diwajibkan melakukan pendaftaran ke pengelola
							<span style="font-weight: bold;">DesaWisata</span>. Setelah status Penjual aktif, Pengguna berhak melakukan
							pengaturan terhadap item-item yang akan diperdagangkan. Pengelola
							<span style="font-weight: bold;">DesaWisata</span> akan melakukan penilaian terhadap item yang dibuat oleh
							Penjual. Setelah persetujuan diberikan, item tersebut baru dapat ditransaksikan.
							<br> </li>
						<li>
							<strong>DesaWisata</strong> tanpa pemberitahuan terlebih dahulu kepada Pengguna, memiliki kewenangan untuk
							melakukan tindakan yang perlu atas setiap dugaan pelanggaran atau pelanggaran Syarat &amp; ketentuan dan/atau
							hukum yang berlaku, yakni tindakan
							berupa penghapusan Barang, moderasi Penjual, pembatalan listing, suspensi akun, dan/atau penghapusan akun
							pengguna.</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan untuk menutup akun Pengguna baik sementara maupun permanen apabila
							didapati adanya tindakan kecurangan dalam bertransaksi dan/atau pelanggaran terhadap syarat dan ketentuan
							<strong>DesaWisata</strong>.</li>
						<li>Pengguna dilarang untuk menciptakan dan/atau menggunakan perangkat, software, fitur dan/atau alat lainnya
							yang bertujuan untuk melakukan manipulasi pada sistem
							<strong>DesaWisata</strong>, termasuk namun tidak terbatas pada : (i) manipulasi data item produk; (ii) kegiatan
							perambanan (crawling/scraping); (iii) kegiatan otomatisasi dalam transaksi, jual beli, promosi, dsb; (v)
							penambahan produk ke katalog;
							dan/atau (vi) aktivitas lain yang secara wajar dapat dinilai sebagai tindakan manipulasi sistem.</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan untuk melakukan penyesuaian jumlah transaksi, penyesuaian jumlah
							reputasi, dan/atau melakukan proses moderasi/menutup akun Pengguna, jika diketahui atau diduga adanya kecurangan
							oleh Pengguna yang bertujuan
							memanipulasi data transaksi Pengguna demi meningkatkan reputasi Pengguna (review dan atau jumlah transaksi).
							Contohnya adalah melakukan proses belanja untuk item produk sendiri dengan menggunakan akun pribadi atau akun
							pribadi lainnya.</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan untuk melakukan pembekuan saldo
							<strong>DesaWisata</strong> Pengguna apabila ditemukan / diduga adanya tindak kecurangan dalam bertransaksi
							dan/atau pelanggaran terhadap syarat dan ketentuan
							<strong>DesaWisata</strong>.</li>
						<li>Penjual dilarang melakukan duplikasi Pengguna, duplikasi produk, atau tindakan-tindakan lain yang dapat
							diindikasikan sebagai usaha persaingan tidak sehat.</li>
						<li>Pengguna tidak memiliki hak untuk mengubah nama akun Pengguna.</li>
						<li>Pengguna bertanggung jawab secara pribadi untuk menjaga kerahasiaan akun dan password untuk semua aktivitas
							yang terjadi dalam akun Pengguna.</li>
						<li>
							<strong>DesaWisata</strong> tidak akan meminta username, password maupun kode SMS verifikasi atau kode OTP milik
							akun Pengguna untuk alasan apapun, oleh karena itu
							<strong>DesaWisata</strong> atau pihak lain yang tidak dapat dijamin keamanannya.</li>
						<li>Pengguna setuju untuk memastikan bahwa Pengguna keluar dari akun di akhir setiap sesi dan memberitahu
							<strong>DesaWisata</strong> jika ada penggunaan tanpa izin atas sandi atau akun Pengguna.</li>
						<li>Pengguna dengan ini menyatakan bahwa
							<strong>DesaWisata</strong> tidak bertanggung jawab atas kerugian atau kerusakan yang timbul dari penyalahgunaan
							akun Pengguna.</li>
						<li>Penjual dilarang mempromosikan Barang secara langsung menggunakan fasilitas pesan pribadi, diskusi produk,
							ulasan produk yang dapat mengganggu kenyamanan Pengguna lain.</li>
					</ol>
					<h3>C. TRANSAKSI PEMBELIAN</h3>
					<ol>
						<li>Pembeli wajib bertransaksi melalui prosedur transaksi yang telah ditetapkan oleh
							<strong>DesaWisata</strong>. Pembeli melakukan pembayaran dengan menggunakan metode pembayaran yang sebelumnya
							telah dipilih oleh Pembeli, dan kemudian
							<strong>DesaWisata</strong> akan meneruskan dana ke pihak Penjual apabila tahapan transaksi jual beli pada sistem
							<strong>DesaWisata</strong> telah selesai.</li>
						<li>Saat melakukan pembelian Barang, Pembeli menyetujui bahwa:</li>
						<ol>
							<li>Pembeli bertanggung jawab untuk membaca, memahami, dan menyetujui informasi/deskripsi keseluruhan Barang
								(termasuk didalamnya namun tidak terbatas pada warna, kualitas, fungsi, dan lainnya) sebelum membuat komitmen
								untuk membeli.</li>
						</ol>
						<ol>
							<li>Pembeli mengakui bahwa warna sebenarnya dari produk sebagaimana terlihat di situs
								<strong>DesaWisata</strong> tergantung pada monitor komputer Pembeli.
								<strong>DesaWisata</strong> telah melakukan upaya terbaik untuk memastikan warna dalam foto-foto yang ditampilkan
								di Situs
								<strong>DesaWisata</strong> muncul seakurat mungkin, tetapi tidak dapat menjamin bahwa penampilan warna pada
								Situs
								<strong>DesaWisata</strong> akan akurat.</li>
						</ol>
						<ol>
							<li>Pengguna masuk ke dalam kontrak yang mengikat secara hukum untuk membeli Barang ketika Pengguna membeli
								suatu barang.</li>
						</ol>
						<ol>
							<li>
								<strong>DesaWisata</strong> tidak mengalihkan kepemilikan secara hukum atas barang-barang dari Penjual kepada
								Pembeli.</li>
						</ol>
						<li>Pembeli memahami dan menyetujui bahwa ketersediaan stok Barang merupakan tanggung jawab Penjual yang
							menawarkan Barang tersebut. Terkait ketersediaan stok Barang dapat berubah sewaktu-waktu, sehingga dalam keadaan
							stok Barang kosong, maka
							penjual akan menolak order, dan pembayaran atas barang yang bersangkutan dikembalikan kepada Pembeli.</li>
						<li>Pembeli memahami sepenuhnya dan menyetujui bahwa segala transaksi yang dilakukan antar Pembeli dan Penjual
							selain melalui Rekening Resmi
							<strong>DesaWisata</strong> dan/atau tanpa sepengetahuan
							<strong>DesaWisata</strong> (melalui fasilitas/jaringan pribadi, pengiriman pesan, pengaturan transaksi khusus
							diluar situs
							<strong>DesaWisata</strong> atau upaya lainnya) adalah merupakan tanggung jawab pribadi dari Pembeli.</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan sepenuhnya untuk menolak pembayaran tanpa pemberitahuan terlebih
							dahulu.</li>
						<li>Pembayaran oleh Pembeli wajib dilakukan segera (selambat-lambatnya dalam batas waktu 2 hari) setelah Pembeli
							melakukan check-out. Jika dalam batas waktu tersebut pembayaran atau konfirmasi pembayaran belum dilakukan oleh
							pembeli,
							<strong>DesaWisata</strong> memiliki kewenangan untuk membatalkan transaksi dimaksud. Pengguna tidak berhak
							mengajukan klaim atau tuntutan atas pembatalan transaksi tersebut.</li>
						<li>Konfirmasi pembayaran dengan setoran tunai wajib disertai dengan berita pada slip setoran berupa nomor
							pesanan dan nama. Konfirmasi pembayaran dengan setoran tunai tanpa keterangan tidak akan diproses oleh
							<strong>DesaWisata</strong>.</li>
						<li>Pembeli menyetujui untuk tidak memberitahukan atau menyerahkan bukti pembayaran dan/atau data pembayaran
							kepada pihak lain selain
							<strong>DesaWisata</strong>. Dalam hal terjadi kerugian akibat pemberitahuan atau penyerahan bukti pembayaran
							dan/atau data pembayaran oleh Pembeli kepada pihak lain, maka hal tersebut akan menjadi tanggung jawab Pembeli.</li>
						<li>Pembeli wajib melakukan konfirmasi penerimaan Barang, setelah menerima kiriman Barang yang dibeli.
							<strong>DesaWisata</strong> memberikan batas waktu 2 (dua) hari setelah pengiriman berstatus "terkirim" pada
							sistem
							<strong>DesaWisata</strong>, untuk Pembeli melakukan konfirmasi penerimaan Barang. Jika dalam batas waktu tersebut
							tidak ada konfirmasi atau klaim dari pihak Pembeli, maka dengan demikian Pembeli menyatakan menyetujui
							dilakukannya konfirmasi penerimaan
							Barang secara otomatis oleh sistem
							<strong>DesaWisata</strong>.</li>
						<li>Setelah adanya konfirmasi penerimaan Barang atau konfirmasi penerimaan Barang otomatis, maka dana pihak
							Pembeli yang dikirimkan ke Rekening resmi
							<strong>DesaWisata</strong> akan di lanjut kirimkan ke pihak Penjual (transaksi dianggap selesai).</li>
						<li>Pembeli memahami dan menyetujui bahwa setiap klaim yang dilayangkan setelah adanya konfirmasi / konfirmasi
							otomatis penerimaan Barang adalah bukan menjadi tanggung jawab
							<strong>DesaWisata</strong>. Kerugian yang timbul setelah adanya konfirmasi/konfirmasi otomatis penerimaan Barang
							menjadi tanggung jawab Pembeli secara pribadi.</li>
						<li>Pembeli memahami dan menyetujui bahwa setiap masalah pengiriman Barang yang disebabkan keterlambatan
							pembayaran adalah merupakan tanggung jawab dari Pembeli.</li>
						<li>Pembeli memahami dan menyetujui bahwa masalah keterlambatan proses pembayaran dan biaya tambahan yang
							disebabkan oleh perbedaan bank yang Pembeli pergunakan dengan bank Rekening resmi
							<strong>DesaWisata</strong> adalah tanggung jawab Pembeli secara pribadi.</li>
						<li>Pengembalian dana dari
							<strong>DesaWisata</strong> kepada Pembeli hanya dapat dilakukan jika dalam keadaan-keadaan tertentu berikut ini:</li>
						<ol>
							<li>Kelebihan pembayaran dari Pembeli atas harga Barang,</li>
						</ol>
						<ol>
							<li>Masalah pengiriman Barang telah teridentifikasi secara jelas dari Penjual yang mengakibatkan pesanan Barang
								tidak sampai,</li>
						</ol>
						<ol>
							<li>Penjual tidak bisa menyanggupi order karena kehabisan stok, perubahan ongkos kirim, maupun penyebab lainnya,</li>
						</ol>
						<ol>
							<li>Penjual sudah menyanggupi pengiriman order Barang, tetapi setelah batas waktu yang ditentukan ternyata
								Penjual tidak mengirimkan Barang hingga batas waktu yang telah ditentukan.</li>
						</ol>
						<ol>
							<li>Penyelesaian permasalahan dengan keputusan untuk pengembalian dana kepada Pembeli atau hasil keputusan dari
								pihak
								<strong>DesaWisata</strong>.</li>
						</ol>
						<li>Apabila terjadi proses pengembalian dana, maka pengembalian akan dilakukan setelah Pengguna mengkonfirmasi
							rekening penampung dengan nama pemilik rekening sama dengan nama Pengguna yang terdaftar di
							<strong>DesaWisata.</strong> Setelah
							<span style="font-weight: bold;">DesaWisata</span> melakukan verifikasi dan menyetujui kepemilikan, maka dana akan
							ditransfer ke rekening milik Pengguna sesuai dengan jumlah pengembalian dana. Jika Pengguna menggunakan pilihan
							metode pembayaran kartu kredit maka pengembalian
							dana akan merujuk pada ketentuan di bagian Kartu Kredit.</li>
						<li>
							<strong>DesaWisata</strong> berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum
							terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Penjual dan Pembeli, dengan melihat
							bukti-bukti yang ada. Keputusan
							<strong>DesaWisata</strong> adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Penjual dan
							Pembeli untuk mematuhinya.</li>
						<li>Apabila Pembeli memilih menggunakan metode pembayaran transfer bank, maka total pembayaran akan ditambahkan
							kode unik untuk mempermudah proses verifikasi. Dalam hal pembayaran telah diverifikasi maka kode unik akan
							dikembalikan ke Saldo
							<strong>DesaWisata</strong> Pembeli.</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan melakukan perubahan status pemesanan menjadi "terkirim" apabila
							tidak ada pembaharuan status pengiriman dari kurir setelah 10 hari sejak resi diinput oleh Penjual dan tidak ada
							konfirmasi lebih lanjut
							dari pihak Pembeli perihal barang pesanan. Kemudian dalam jangka waktu 5 hari sejak perubahan status tersebut
							diatas,
							<strong>DesaWisata</strong> memberikan kesempatan kepada pihak Pembeli untuk melakukan (i) konfirmasi penerimaan
							barang atau (ii) Komplain. Jika dalam jangka waktu 5 hari tersebut tidak ada konfirmasi penerimaan barang atau
							komplain apapun dari Pembeli,
							maka
							<strong>DesaWisata</strong> memiliki kewenangan untuk menyelesaikan transaksi dan meneruskan dana kepada Penjual
							bersangkutan yang dianggap telah melakukan kewajibannya mengirimkan barang dan menginformasikan nomor resi
							pengiriman. Penyesuaian status
							pemesanan ini hanya akan dilakukan apabila alamat tujuan pengiriman yang tertera pada invoice pemesanan dan resi
							kurir telah sesuai.</li>
						<li>Pembeli memahami sepenuhnya dan menyetujui bahwa invoice yang diterbitkan adalah atas nama Penjual.</li>
					</ol>
					<h3>D. TRANSAKSI PENJUALAN</h3>
					<ol>
						<li>Penjual dilarang memanipulasi harga Barang dengan tujuan apapun.</li>
						<li>Penjual dilarang melakukan penawaran / berdagang Barang terlarang sesuai dengan yang telah ditetapkan pada
							ketentuan "Jenis Barang".</li>
						<li>Penjual wajib memberikan foto dan informasi produk dengan lengkap dan jelas sesuai dengan kondisi dan
							kualitas produk yang dijualnya. Apabila terdapat ketidaksesuaian antara foto dan informasi produk yang diunggah
							oleh Penjual dengan produk
							yang diterima oleh Pembeli, maka
							<strong>DesaWisata</strong> berhak membatalkan/menahan dana transaksi.</li>
						<li>Dalam menggunakan Fasilitas "Judul Produk", "Foto Produk", "Catatan" dan "Deskripsi Produk", Penjual dilarang
							membuat peraturan bersifat klausula baku yang tidak memenuhi peraturan perundang-undangan yang berlaku di
							Indonesia, termasuk namun
							tidak terbatas pada (i) tidak menerima komplain, (ii) tidak menerima retur (penukaran barang), (iii) tidak
							menerima refund (pengembalian dana), (iv) barang tidak bergaransi, (v) pengalihan tanggung jawab (termasuk tidak
							terbatas pada penanggungan
							ongkos kirim), (vi) penyusutan nilai harga dan (vii) pengiriman barang acak secara sepihak. Jika terdapat
							pertentangan antara catatan toko dan/atau deskripsi produk dengan Syarat &amp; Ketentuan
							<strong>DesaWisata</strong>, maka peraturan yang berlaku adalah Syarat &amp; Ketentuan
							<strong>DesaWisata</strong>.</li>
						<li>Penjual wajib memberikan balasan untuk menerima atau menolak pesanan Barang pihak Pembeli dalam batas waktu 2
							hari terhitung sejak adanya notifikasi pesanan Barang dari
							<strong>DesaWisata</strong>. Jika dalam batas waktu tersebut tidak ada balasan dari Penjual maka secara otomatis
							pesanan dianggap dibatalkan.</li>
						<li>Penjual wajib memasukan nomor resi pengiriman Barang dalam batas waktu 4 x 24 jam (tidak termasuk hari
							Sabtu/Minggu/libur Nasional) terhitung sejak adanya notifikasi pesanan Barang dari
							<strong>DesaWisata</strong>. Jika dalam batas waktu tersebut pihak Penjual tidak memasukan nomor resi pengiriman
							Barang maka secara otomatis pesanan dianggap dibatalkan. Jika Penjual tetap mengirimkan Barang setelah melebihi
							batas waktu pengiriman
							sebagaimana dijelaskan diatas, maka Penjual memahami bahwa transaksi akan tetap dibatalkan untuk kemudian
							Penjual dapat melakukan penarikan Barang pada kurir tempat Barang dikirimkan.</li>
						<li>Penjual memahami dan menyetujui bahwa pembayaran atas harga Barang dan ongkos kirim (diluar biaya transfer /
							administrasi) akan dikembalikan sepenuhnya ke Pembeli apabila transaksi dibatalkan dan/atau transaksi tidak
							berhasil dan/atau ketentuan
							lain yang diatur dalam Syarat &amp; Ketentuan Poin C. 14.</li>
						<li>Dalam keadaan Penjual hanya dapat memenuhi sebagian dari jumlah Barang yang dipesan oleh Pembeli, maka
							Penjual wajib memberikan keterangan kepada
							<strong>DesaWisata.</strong>
						</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan untuk menahan pembayaran dana di Rekening Resmi
							<strong>DesaWisata</strong> sampai waktu yang tidak ditentukan apabila terdapat permasalahan dan klaim dari pihak
							Pembeli terkait proses pengiriman dan kualitas Barang. Pembayaran baru akan dilanjut kirimkan kepada Penjual
							apabila permasalahan tersebut
							telah selesai dan/atau Barang telah diterima oleh Pembeli.</li>
						<li>
							<strong>DesaWisata</strong> berwenang untuk membatalkan dan/atau menahan dana transaksi dalam hal: (i) Nomor resi
							kurir pengiriman barang yang diberikan oleh Penjual tidak sesuai dan/atau diduga tidak sesuai dengan transaksi
							yang terjadi di Situs
							<strong>DesaWisata</strong>; (ii) Penjual mengirimkan barang melalui jasa kurir/logistik selain dari yang
							disediakan dan terhubung dengan Situs
							<strong>DesaWisata</strong>; (iii)
							<strong>DesaWisata</strong> berhak membatalkan/menahan dana transaksi jika nama produk dan deskripsi produk tidak
							sesuai/tidak jelas dengan produk yang dikirim.</li>
						<li>Penjual memahami dan menyetujui bahwa Pajak Penghasilan Penjual akan dilaporkan dan diurus sendiri oleh
							masing-masing Penjual sesuai dengan ketentuan pajak yang berlaku di peraturan perundang-undangan di Indonesia.</li>
						<li>
							<strong>DesaWisata</strong> berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum
							terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Penjual dan Pembeli, dengan melihat
							bukti-bukti yang ada. Keputusan
							<strong>DesaWisata</strong> adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Penjual dan
							Pembeli untuk mematuhinya.</li>
						<li>Apabila disepakati oleh Penjual dan Pembeli, penggunaan jasa Logistik yang berbeda dari pilihan awal pembeli
							dapat dilakukan (dengan ketentuan bahwa tarif pengiriman tersebut adalah dibawah tarif pengiriman awal).</li>
						<li>
							<strong>DesaWisata</strong> berwenang memotong kelebihan tarif pengiriman dari dana pembayaran pembeli dan
							mengembalikan selisih kelebihan tarif pengiriman kepada pembeli.</li>
						<li>Penjual memahami sepenuhnya dan menyetujui bahwa invoice yang diterbitkan adalah atas nama Penjual.</li>
					</ol>
					<h3>E. PENATAAN ETALASE</h3>
					<ol>
						<li>Penjual dilarang mempergunakan katalog
							<span style="font-weight: bold;">DesaWisata</span> (termasuk dan tidak tebatas pada informasi Penjual dan
							informasi Barang) sebagai media untuk beriklan atau melakukan promosi ke halaman situs lain diluar situs
							<strong>DesaWisata</strong>.</li>
						<li>Penjual dilarang memberikan data kontak pribadi dengan maksud untuk melakukan transaksi secara langsung
							kepada Pembeli / calon Pembeli.</li>
						<li>Penjual dilarang memberikan keterangan (informasi Penjual dan/atau Barang) selain/diluar daripada keterangan
							Penjual dan/atau Barang yang bersangkutan.</li>
						<li>Penamaan Barang harus dilakukan sesuai dengan informasi detail, spesifikasi, dan kondisi Barang, dengan
							demikian Pengguna tidak diperkenankan untuk mencantumkan nama dan/atau kata yang tidak berkaitan dengan Barang
							tersebut.</li>
						<li>Penamaan Barang dan informasi produk harus sesuai dengan kondisi Barang yang ditampilkan dan Pengguna tidak
							diperkenankan mencantumkan nama dan informasi yang tidak sesuai dengan kondisi Barang.</li>
						<li>Penjual wajib memisahkan tiap-tiap Barang yang memiliki ukuran dan harga yang berbeda.</li>
						<li>Penjual tidak diperkenankan memperdagangkan jasa, atau Barang non-fisik.</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan mengambil-alih nama akun Penjual jika akun Penjual sudah tidak
							aktif lebih dari 9 bulan, dan/atau pemilik merek dagang resmi (sesuai dengan Daftar Umum Merek di Indonesia)
							dengan nama yang sama dengan
							nama Akun Penjual melakukan klaim terhadapnya dikarenakan mereka ingin menggunakan nama tersebut.</li>
						<li>
							<strong>DesaWisata</strong> memiliki kewenangan untuk mengubah nama dan/atau memakai nama Penjual dan/atau domain
							Pengguna untuk kepentingan internal
							<strong>DesaWisata</strong>.</li>
					</ol>
					<h3>F. KOMISI</h3>
					<ol>
						<li>
							<strong>DesaWisata</strong> memberlakukan sistem komisi untuk setiap transaksi yang dilakukan melalui Rekening
							Resmi
							<strong>DesaWisata</strong>.</li>
						<li>Sistem Komisi
							<span style="font-weight: bold;"> DesaWisata</span> berlaku sesuai dengan Syarat dan Ketentuan Khusus
							<span style="font-weight: bold;">DesaWisata</span> dengan Penjual.</li>
					</ol>
					<h3>G. HARGA</h3>
					<ol>
						<li>Harga Barang yang terdapat dalam situs
							<strong>DesaWisata</strong> adalah harga yang ditetapkan oleh Penjual. Penjual dilarang memanipulasi harga barang
							dengan cara apapun.</li>
						<li>Pembeli memahami dan menyetujui bahwa kesalahan keterangan harga dan informasi lainnya yang disebabkan tidak
							terbaharuinya halaman situs
							<strong>DesaWisata</strong> dikarenakan browser/ISP yang dipakai Pembeli adalah tanggung jawab Pembeli.</li>
						<li>Penjual memahami dan menyetujui bahwa kesalahan ketik yang menyebabkan keterangan harga atau informasi lain
							menjadi tidak benar/sesuai adalah tanggung jawab Penjual. Perlu diingat dalam hal ini, apabila terjadi kesalahan
							pengetikan keterangan
							harga Barang yang tidak disengaja, Penjual berhak menolak pesanan Barang yang dilakukan oleh Pembeli.</li>
						<li>Pengguna memahami dan menyetujui bahwa setiap masalah dan/atau perselisihan yang terjadi akibat
							ketidaksepahaman antara Penjual dan Pembeli tentang harga bukanlah merupakan tanggung jawab
							<strong>DesaWisata</strong>.</li>
						<li>Dengan melakukan pemesanan melalui
							<strong>DesaWisata</strong>, Pengguna menyetujui untuk membayar total biaya yang harus dibayarkan sebagaimana
							tertera dalam halaman pembayaran, yang terdiri dari harga barang, ongkos kirim, dan biaya-biaya lain yang
							mungkin timbul dan akan diuraikan
							secara tegas dalam halaman pembayaran. Pengguna setuju untuk melakukan pembayaran melalui metode pembayaran yang
							telah dipilih sebelumnya oleh Pengguna.</li>
						<li>Batasan harga maksimal satuan untuk Barang yang dapat ditawarkan adalah Rp. 100.000.000,-</li>
						<li>Situs
							<strong>DesaWisata</strong> untuk saat ini hanya melayani transaksi jual beli Barang dalam mata uang Rupiah.</li>
					</ol>
					<h3>H. TARIF PENGIRIMAN</h3>
					<ol>
						<li>Pembeli memahami dan mengerti bahwa
							<strong>DesaWisata</strong> telah melakukan usaha sebaik mungkin dalam memberikan informasi tarif pengiriman
							kepada Pembeli berdasarkan lokasi secara akurat, namun
							<strong>DesaWisata</strong> tidak dapat menjamin keakuratan data tersebut dengan yang ada pada cabang setempat.</li>
						<li>Karena itu
							<strong>DesaWisata</strong> menyarankan kepada Penjual untuk mencatat terlebih dahulu tarif yang diberikan
							<strong>DesaWisata</strong>, agar dapat dibandingkan dengan tarif yang dibebankan di cabang setempat. Apabila
							mendapati perbedaan, mohon sekiranya untuk menginformasikan kepada kami melalui halaman "Hubungi Kami" dengan
							memberikan data harga yang
							didapat beserta kota asal dan tujuan, agar dapat kami telusuri lebih lanjut.</li>
						<li>Pengguna memahami dan menyetujui bahwa selisih biaya pengiriman Barang adalah di luar tanggung jawab
							<strong>DesaWisata</strong>, dan oleh karena itu, adalah kebijakan Penjual sendiri untuk membatalkan atau tetap
							melakukan pengiriman Barang.</li>
					</ol>
					<h3>I. KONTEN</h3>
					<ol>
						<li>Dalam menggunakan setiap fitur dan/atau layanan
							<strong>DesaWisata</strong>, Pengguna dilarang untuk mengunggah atau mempergunakan kata-kata, komentar, gambar,
							atau konten apapun yang mengandung unsur SARA, diskriminasi, merendahkan atau menyudutkan orang lain, vulgar,
							bersifat ancaman, atau hal-hal
							lain yang dapat dianggap tidak sesuai dengan nilai dan norma sosial.
							<strong>DesaWisata</strong> berhak melakukan tindakan yang diperlukan atas pelanggaran ketentuan ini, antara lain
							penghapusan konten, moderasi akun Penjual, pemblokiran akun, dan lain-lain.</li>
						<li>Pengguna dilarang mempergunakan foto/gambar Barang yang memiliki watermark yang menandakan hak kepemilikan
							orang lain.</li>
						<li>Penguna dengan ini memahami dan menyetujui bahwa penyalahgunaan foto/gambar yang di unggah oleh Pengguna
							adalah tanggung jawab Pengguna secara pribadi.</li>
						<li>Penjual tidak diperkenankan untuk mempergunakan foto/gambar Barang atau logo toko sebagai media untuk
							beriklan atau melakukan promosi ke situs-situs lain diluar Situs
							<strong>DesaWisata</strong>, atau memberikan data kontak pribadi untuk melakukan transaksi secara langsung kepada
							pembeli / calon pembeli.</li>
						<li>Ketika Pengguna menggungah ke Situs
							<strong>DesaWisata</strong> dengan konten atau posting konten, Pengguna memberikan
							<strong>DesaWisata</strong> hak non-eksklusif, di seluruh dunia, secara terus-menerus, tidak dapat dibatalkan,
							bebas royalti, disublisensikan ( melalui beberapa tingkatan ) hak untuk melaksanakan setiap dan semua hak cipta,
							publisitas , merek dagang
							, hak basis data dan hak kekayaan intelektual yang Pengguna miliki dalam konten, di media manapun yang dikenal
							sekarang atau di masa depan. Selanjutnya , untuk sepenuhnya diizinkan oleh hukum yang berlaku , Anda
							mengesampingkan hak moral
							dan berjanji untuk tidak menuntut hak-hak tersebut terhadap
							<strong>DesaWisata</strong>.</li>
						<li>Pengguna menjamin bahwa tidak melanggar hak kekayaan intelektual dalam mengunggah konten Pengguna kedalam
							situs
							<strong>DesaWisata</strong>. Setiap Pengguna dengan ini bertanggung jawab secara pribadi atas pelanggaran hak
							kekayaan intelektual dalam mengunggah konten di Situs
							<strong>DesaWisata</strong>.</li>
						<li>Meskipun kami mencoba untuk menawarkan informasi yang dapat diandalkan, kami tidak bisa menjanjikan bahwa
							katalog akan selalu akurat dan terbarui, dan Pengguna setuju bahwa Pengguna tidak akan meminta
							<strong>DesaWisata</strong> bertanggung jawab atas ketimpangan dalam katalog. Katalog mungkin termasuk hak cipta,
							merek dagang atau hak milik lainnya.</li>
					</ol>
					<h3>J. JENIS BARANG</h3> Berikut ini adalah daftar jenis Barang yang dilarang untuk diperdagangkan oleh Penjual
					pada Situs
					<strong>DesaWisata</strong>:
					<br>
					<br>
					<ol>
						<li>Segala jenis obat-obatan maupun zat-zat lain yang dilarang ataupun dibatasi peredarannya menurut ketentuan
							hukum yang berlaku, termasuk namun tidak terbatas pada ketentuan Undang-Undang Narkotika, Undang-Undang
							Psikotropika, dan Undang-Undang
							Kesehatan. Termasuk pula dalam ketentuan ini ialah obat keras, obat-obatan yang memerlukan resep dokter, obat
							bius dan sejenisnya, atau obat yang tidak memiliki izin edar dari Badan Pengawas Obat dan Makanan (BPOM).</li>
						<li>Kosmetik dan makanan minuman yang membahayakan keselamatan penggunanya, ataupun yang tidak mempunyai izin
							edar dari Badan Pengawas Obat dan Makanan (BPOM).</li>
						<li>Bahan yang diklasifikasikan sebagai Bahan Berbahaya menurut Peraturan Menteri Perdagangan yang berlaku.</li>
						<li>Jenis Produk tertentu yang wajib memiliki (namun yang diperjual-belikan tidak mencantumkan):</li>
						<ol>
							<li>SNI;</li>
							<li>Petunjuk penggunaan dalam Bahasa Indonesia; atau</li>
							<li>Label dalam Bahasa Indonesia.</li>
						</ol>
						<li>Barang-barang lain yang kepemilikannya ataupun peredarannya melanggar ketentuan hukum yang berlaku di
							Indonesia.</li>
						<li>Media berbentuk CD/DVD/VCD, atau media rekam lain yang bertentangan dengan undang-undang hak cipta. Termasuk
							di dalamnya adalah yang memuat film, musik, permainan, atau perangkat lunak bajakan.</li>
						<li>Barang dewasa penunjang kegiatan seksual termasuk namun tidak terbatas pada obat kuat, obat perangsang, alat
							bantu seks, pornografi, dan obat-obatan dewasa, kecuali untuk alat kesehatan (kontrasepsi) yang diizinkan untuk
							diperjual belikan
							oleh peraturan hukum yang berlaku.</li>
						<li>Minuman beralkohol.</li>
						<li>Iklan.</li>
						<li>Segala bentuk tulisan yang dapat berpengaruh negatif terhadap pemakaian situs ini.</li>
						<li>Pakaian dalam bekas.</li>
						<li>Senjata api, senjata tajam, senapan angin, dan segala macam senjata.</li>
						<li>Dokumen pemerintahan dan perjalanan.</li>
						<li>Seragam pemerintahan.</li>
						<li>Bagian/Organ manusia.</li>
						<li>Mailing list dan informasi pribadi.</li>
						<li>Barang-Barang yang melecehkan pihak/ras tertentu atau dapat merendahkan martabat orang lain.</li>
						<li>Pestisida.</li>
						<li>Atribut kepolisian.</li>
						<li>Barang hasil tindak pencurian.</li>
						<li>Pembuka kunci dan segala aksesori penunjang tindakan perampokan/pencurian.</li>
						<li>Barang yang dapat dan atau mudah meledak, menyala atau terbakar sendiri.</li>
						<li>Barang cetakan/rekaman yang isinya dapat mengganggu keamanan &amp; ketertiban serta stabilitas nasional.</li>
						<li>Hewan.</li>
						<li>Uang tunai.</li>
						<li>Materai.</li>
						<li>Pengacak sinyal, penghilang sinyal, dan/atau alat-alat lain yang dapat mengganggu sinyal atau jaringan
							telekomunikasi</li>
						<li>Perlengkapan dan peralatan judi.</li>
						<li>Jimat-jimat, benda-benda yang diklaim berkekuatan gaib dan memberi ilmu kesaktian.</li>
						<li>Barang dengan hak Distribusi Eksklusif yang hanya dapat diperdagangkan dengan sistem penjualan lansung oleh
							penjual resmi dan/atau Barang dengan sistem penjualan Multi Level Marketing.</li>
						<li>Produk non fisik yang tidak dapat dikirimkan melalui jasa kurir, termasuk namun tidak terbatas pada produk
							pulsa/voucher (i) telepon, (ii) listrik, (iii) game, (iv) credit digital.</li>
						<li>Tiket Kereta Api.</li>
						<li>Dokumen-dokumen resmi seperti Sertifikat Toefl, Ijazah, Surat Dokter, Kwitansi, dan lain sebagainya</li>
						<li>Segala jenis Barang lain yang bertentangan dengan peraturan pengiriman Barang Indonesia.</li>
						<li>Barang-Barang lain yang melanggar ketentuan hukum yang berlaku di Indonesia.</li>
					</ol>
					<h3>K. KARTU KREDIT</h3>
					<ol>
						<li>Pengguna dapat memilih untuk mempergunakan pilihan metode pembayaran menggunakan kartu kredit untuk transaksi
							pembelian barang melalui Situs
							<strong>DesaWisata</strong>.</li>
						<li>Transaksi pembelian barang dengan menggunakan kartu kredit dapat dilakukan untuk transaksi pembelian dengan
							nilai total belanja minimal Rp. 50.000 (lima puluh ribu rupiah) dan maksimal Rp 50.000.000 (lima puluh juta
							rupiah).</li>
						<li>Transaksi pembelian barang dengan menggunakan kartu kredit&nbsp; wajib mengikuti syarat dan ketentuan yang
							diatur oleh
							<strong>DesaWisata</strong> dan mempergunakan kurir/logistik yang disediakan dan terhubung dengan Situs
							<strong>DesaWisata</strong>.</li>
						<li>Pengguna dilarang untuk mempergunakan metode pembayaran kartu kredit di luar peruntukan sebagai alat
							pembayaran.</li>
						<li>Apabila terdapat transaksi pembelian barang dengan menggunakan kartu kredit yang melanggar ketentuan hukum
							dan/atau syarat ketentuan
							<strong>DesaWisata</strong>, maka
							<strong>DesaWisata</strong>
						</li>
						<ol>
							<li>menahan dana transaksi selama diperlukan oleh
								<strong>DesaWisata</strong>, pihak Bank, maupun mitra payment gateway terkait untuk melakukan investigasi yang
								diperlukan, sekurang-kurangnya 28 hari kerja;</li>
						</ol>
						<ol>
							<li>membatalkan transaksi dan melakukan pemotongan dana sebesar 15% (lima belas persen) dari nilai transaksi,
								serta menarik kembali nilai subsidi sehubungan penggunaan kartu kredit.</li>
						</ol>
						<li>
							<strong>DesaWisata</strong> akan menghentikan kerjasama dengan Penjual yang melakukan tindakan yang dapat
							merugikan prinsipal, penerbit, acquirer dan /atau pemegang kartu kredit, antara lain memproses penarikan / gesek
							tunai (cash withdrawal transaction).</li>
						<li>Apabila transaksi pembelian tidak berhasil dan/atau dibatalkan, maka tagihan atas transaksi tersebut akan
							dibatalkan dan dana transaksi akan dikembalikan ke limit kartu kredit pembeli di tagihan berikutnya. Ketentuan
							pada ayat ini tidak berlaku
							untuk transaksi pembelian barang dengan menggunakan kartu kredit yang melanggar ketentuan hukum dan/atau syarat
							ketentuan
							<strong>DesaWisata</strong>.</li>
						<li>Kartu kredit yang dapat digunakan untuk bertransaksi di
							<strong>DesaWisata</strong> hanya kartu kredit yang diterbitkan di Indonesia.</li>
						<li>Transaksi menggunakan metode pembayaran Kartu Kredit akan dikenakan biaya administrasi sebesar 1,5% dari
							total biaya yang harus dibayarkan. Apabila seluruh transaksi dalam satu pembayaran yang menggunakan kartu kredit
							dibatalkan, maka biaya
							administrasi tersebut akan dikembalikan ke limit kartu kredit Pengguna.</li>
					</ol>
					<h3>L. PROMO</h3>
					<ol>
						<li>
							<strong>DesaWisata</strong> sewaktu-waktu dapat mengadakan kegiatan promosi (selanjutnya disebut sebagai “Promo”)
							dengan Syarat dan Ketentuan yang mungkin berbeda pada masing-masing kegiatan Promo. Pengguna dihimbau untuk
							membaca dengan seksama Syarat
							dan Ketentuan Promo tersebut.</li>
						<li> Apabila terdapat transaksi Promo yang mempergunakan metode pembayaran kartu kredit yang melanggar Syarat dan
							Ketentuan
							<strong>DesaWisata</strong>, maka akan merujuk pada Syarat dan Ketentuan Poin K. Kartu Kredit.</li>
						<li>
							<strong>DesaWisata</strong> berhak, tanpa pemberitahuan sebelumnya, melakukan tindakan-tindakan yang diperlukan
							termasuk namun tidak terbatas pada menarik subsidi atau cashback, pencabutan Promo, membatalkan transaksi,
							menahan dana, menurunkan reputasi
							Penjual, menutup akun Penjual, serta hal-hal lainnya jika ditemukan adanya manipulasi, pelanggaran maupun
							pemanfaatan Promo untuk keuntungan pribadi Pengguna, maupun indikasi kecurangan atau pelanggaran pelanggaran
							Syarat dan Ketentuan
							<strong>DesaWisata</strong>
						</li>
					</ol>
					<h3>M. PENGIRIMAN BARANG</h3>
					<ol>
						<li> Pengiriman Barang dalam sistem
							<strong>DesaWisata</strong> wajib menggunakan jasa perusahaan ekspedisi yang telah mendapatkan verifikasi rekanan
							<strong>DesaWisata</strong> yang dipilih oleh Pembeli.</li>
						<li> Penjual dilarang memberlakukan promosi / sistem bebas ongkos kirim pada setiap Barang yang dijual di dalam
							Situs
							<strong>DesaWisata</strong>.</li>
						<li> Setiap ketentuan berkenaan dengan proses pengiriman Barang adalah wewenang sepenuhnya penyedia jasa layanan
							pengiriman Barang.</li>
						<li> Penjual wajib memenuhi ketentuan yang ditetapkan oleh jasa layanan pengiriman barang tersebut dan
							bertanggung jawab atas setiap Barang yang dikirimkan.</li>
						<li> Pengguna memahami dan menyetujui bahwa setiap permasalahan yang terjadi pada saat proses pengiriman Barang
							oleh penyedia jasa layanan pengiriman Barang adalah merupakan tanggung jawab penyedia jasa layanan pengiriman.</li>
						<li> Dalam hal diperlukan untuk dilakukan proses pengembalian barang, maka Pengguna, baik Penjual maupun Pembeli,
							diwajibkan untuk melakukan pengiriman barang langsung ke masing-masing Pembeli maupun Penjual.
							<strong>DesaWisata</strong> tidak menerima pengembalian atau pengiriman barang atas transaksi yang dilakukan oleh
							Pengguna dalam kondisi apapun.</li>
						<li>Dalam hal terjadi kendala dalam proses pengiriman berupa barang hilang, barang rusak, dan lain sebagainya,
							Pembeli dan Penjual dapat melaporkan ke pihak
							<strong>DesaWisata.</strong>
						</li>
					</ol>
					<h3>N. PENOLAKAN JAMINAN DAN BATASAN TANGGUNG JAWAB</h3>
					<strong>DesaWisata</strong> adalah portal web dengan model Costumer to Customer Marketplace, yang menyediakan
					layanan kepada Pengguna untuk dapat menjadi Penjual maupun Pembeli di website
					<strong>DesaWisata</strong>. Dengan demikian transaksi yang terjadi adalah transaksi antar member
					<strong>DesaWisata</strong>, sehingga Pengguna memahami bahwa batasan tanggung jawab
					<strong>DesaWisata</strong> secara proporsional adalah sebagai penyedia jasa portal web
					<br>
					<br>
					<strong>DesaWisata</strong> selalu berupaya untuk menjaga Layanan
					<strong>DesaWisata</strong> aman, nyaman, dan berfungsi dengan baik, tapi kami tidak dapat menjamin operasi
					terus-menerus atau akses ke Layanan kami dapat selalu sempurna. Informasi dan data dalam situs
					<strong>DesaWisata</strong> memiliki kemungkinan tidak terjadi secara real time.
					<br>
					<br> Pengguna setuju bahwa Anda memanfaatkan Layanan
					<strong>DesaWisata</strong> atas risiko Pengguna sendiri, dan Layanan
					<strong>DesaWisata</strong> diberikan kepada Anda pada "SEBAGAIMANA ADANYA" dan "SEBAGAIMANA TERSEDIA".
					<br>
					<br> Sejauh diizinkan oleh hukum yang berlaku,
					<strong>DesaWisata</strong> (termasuk Induk Perusahaan, direktur, dan karyawan) adalah tidak bertanggung jawab, dan
					Anda setuju untuk tidak menuntut
					<strong>DesaWisata</strong> bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas
					pada hilangnya uang, reputasi, keuntungan, atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung
					atau tidak langsung dari
					:
					<br>
					<br>
					<ul>
						<li> Penggunaan atau ketidakmampuan Pengguna dalam menggunakan Layanan
							<strong>DesaWisata</strong>.</li>
						<li>Harga, Pengiriman atau petunjuk lain yang tersedia dalam layanan
							<strong>DesaWisata</strong>.</li>
						<li> Keterlambatan atau gangguan dalam Layanan
							<strong>DesaWisata</strong>.</li>
						<li> Kelalaian dan kerugian yang ditimbulkan oleh masing-masing Pengguna.</li>
						<li>Kualitas Barang.</li>
						<li>Pengiriman Barang.</li>
						<li>Pelanggaran Hak atas Kekayaan Intelektual.</li>
						<li>Perselisihan antar pengguna.</li>
						<li>Pencemaran nama baik pihak lain.</li>
						<li>Setiap penyalahgunaan Barang yang sudah dibeli pihak Pengguna.</li>
						<li>Kerugian akibat pembayaran tidak resmi kepada pihak lain selain ke Rekening Resmi
							<strong>DesaWisata</strong>, yang dengan cara apa pun mengatas-namakan
							<strong>DesaWisata</strong> ataupun kelalaian penulisan rekening dan/atau informasi lainnya dan/atau kelalaian
							pihak bank.</li>
						<li>Pengiriman untuk perbaikan Barang yang bergaransi resmi dari pihak produsen. Pembeli dapat membawa Barang
							langsung kepada pusat layanan servis terdekat dengan kartu garansi dan faktur pembelian.</li>
						<li>Virus atau perangkat lunak berbahaya lainnya yang diperoleh dengan mengakses , atau menghubungkan ke layanan
							<strong>DesaWisata</strong>.</li>
						<li>Gangguan, bug, kesalahan atau ketidakakuratan apapun dalam Layanan
							<strong>DesaWisata</strong>.</li>
						<li>Kerusakan pada perangkat keras Anda dari penggunaan setiap Layanan
							<strong>DesaWisata</strong>.</li>
						<li>Isi, tindakan, atau tidak adanya tindakan dari pihak ketiga, termasuk terkait dengan Produk yang ada dalam
							situs
							<strong>DesaWisata</strong> yang diduga palsu.</li>
						<li>Tindak penegakan yang diambil sehubungan dengan akun Pengguna.</li>
						<li>Adanya tindakan peretasan yang dilakukan oleh pihak ketiga kepada akun pengguna.</li>
					</ul>
					<h3>O. PELEPASAN</h3> Jika Anda memiliki perselisihan dengan satu atau lebih pengguna, Anda melepaskan
					<strong>DesaWisata</strong> (termasuk Induk Perusahaan, Direktur, dan karyawan) dari klaim dan tuntutan atas
					kerusakan dan kerugian (aktual dan tersirat) dari setiap jenis dan sifatnya , yang dikenal dan tidak dikenal, yang
					timbul dari atau dengan cara
					apapun berhubungan dengan sengketa tersebut. Dengan demikian maka Pengguna dengan sengaja melepaskan segala
					perlindungan hukum (yang terdapat dalam undang-undang atau peraturan hukum yang lain) yang akan membatasi cakupan
					ketentuan pelepasan
					ini.
					<br>
					<h3>P. GANTI RUGI</h3> Pengguna akan melepaskan
					<strong>DesaWisata</strong> dari tuntutan ganti rugi dan menjaga
					<strong>DesaWisata</strong> (termasuk Induk Perusahaan, direktur, dan karyawan) dari setiap klaim atau tuntutan,
					termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal Anda melanggar Perjanjian
					ini, penggunaan Layanan
					<strong>DesaWisata</strong> yang tidak semestinya dan/ atau pelanggaran Anda terhadap hukum atau hak-hak pihak
					ketiga.
					<br>
					<h3>Q. PILIHAN HUKUM</h3> Perjanjian ini akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia,
					tanpa memperhatikan pertentangan aturan hukum. Anda setuju bahwa tindakan hukum apapun atau sengketa yang mungkin
					timbul dari, berhubungan
					dengan, atau berada dalam cara apapun berhubungan dengan situs dan/atau Perjanjian ini akan diselesaikan secara
					eksklusif dalam yurisdiksi pengadilan Republik Indonesia.
					<br>
					<h3>R. PEMBAHARUAN</h3> Syarat &amp; ketentuan mungkin di ubah dan/atau diperbaharui dari waktu ke waktu tanpa
					pemberitahuan sebelumnya.
					<strong>DesaWisata</strong> menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat &amp;
					ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan
					layanan
					<strong>DesaWisata</strong>, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat &amp; ketentuan.
				</div>
			</div>
		</div>
	</div>
</section>
