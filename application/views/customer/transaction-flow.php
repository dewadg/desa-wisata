<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<section class="section-red-bg py-5">
	<div class="container">
		<div class="row">
			<div class="col-12 text-white text-center">
				<h1>Aliran Transaksi</h1>
				<h3>Proses bertransaksi di DesaWisata sederhana</h3>
			</div>
		</div>
	</div>
</section>

<section class="bg-white p-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-10">
				<img src="https://www.bumidesa.com/assets/images/aliran_transaksi.jpg" class="img-fluid">
			</div>
		</div>
	</div>
</section>
