$(function(){

    if ($('.card-product-filter').length > 0) {
        product_filter_offset = $('.card-product-filter').offset().top;

         function productFilerFixedTop() {

            if ($('.card-product-filter').length > 0) {

                var offset = product_filter_offset - 100;

                 var scrollTop = $(window).scrollTop();
                if (scrollTop > offset) {
                    $('.card-product-filter-section').addClass('filter-sticky');
                } else {
                    $('.card-product-filter-section').removeClass('filter-sticky');
                }

            }

            return false;
           
        };

        productFilerFixedTop();

        $(window).on('scroll', function () {
            productFilerFixedTop();
        });


    }
    


    function headerFixedTop() {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 50) {
            $('.navbar.main-navbar').addClass('fixed-top');
        } else {
            $('.navbar.main-navbar').removeClass('fixed-top');
        }

    };

    headerFixedTop();

    $(window).on('scroll', function () {
        headerFixedTop();
    });




    $('select.select2').select2();
    $("select.chosen").chosen(); 

    $(".datepicker").datepicker(); 

    $('input.icheck').iCheck({
      checkboxClass: 'icheckbox_flat-red',
      radioClass: 'iradio_flat-red'
    });


    $('#confirmModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) 
      var title = button.data('title');
      var text = button.data('text');
      var action = button.data('action');
  
      var modal = $(this)
      modal.find('.modal-title').text(title);
      modal.find('.modal-text').text(text);
      modal.find('.modal-action').attr('href', action);
    })




    toastr.options = {
      "closeButton": true,
      "positionClass": "toast-top-center",
      "preventDuplicates": true,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
    }



    if ($('.btn-to-top').length) {
      var scrollTrigger = 100, // px
          backToTop = function () {
              var scrollTop = $(window).scrollTop();
              if (scrollTop > scrollTrigger) {
                  $('.btn-to-top').show();
              } else {
                  $('.btn-to-top').hide();
              }
          };
      backToTop();
      $(window).on('scroll', function () {
          backToTop();
      });
      $('.btn-to-top').on('click', function (e) {
          e.preventDefault();
          $('html,body').animate({
              scrollTop: 0
          }, 700);
      });
  }


  $('.topbar-menu  li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).show();
  }, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).hide();
  });



  $('body').on('click','.new-tab-link',function(e){

    e.preventDefault();

    var win = window.open($(this).attr('href'), '_blank');
    win.focus();


  })


  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })


   $(document).on('click', '.dropdown-menu', function (e) {
      e.stopPropagation();
    });

  

})

