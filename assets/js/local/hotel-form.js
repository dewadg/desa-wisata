$(function(){


      $('#hotelSearch').select2({
       placeholder: "Kota, Penginapan, Tujuan",
     });


      var check_in = $('.check-in').datepicker({
      	startDate: "d",
      	maxViewMode: 1,
      })

      var check_out = $('.check-out').datepicker({
      	startDate: "+d",
      	maxViewMode: 1,
      })

      $('.input-daterange').datepicker();

});
      	

var hotel_form = new Vue({

      el: '#hotel-form',

      data:{
            gradeOptions : null,
            grade : null,
            destination : null,
            checkIn : null,
            checkOut : null,
            room : 1,
            adult : 1,
            child : 0,
            guestRule: {
            	maxAdult : 5,
            	minAdult : 1,
            	maxRoom : 5,
            	minRoom : 1,
            	maxChild : 2,
            	minChild : 0,
            }
      },

      methods : {

            guestValidation: function(){

            	if (this.room < this.guestRule.minRoom) {
            		this.room = this.guestRule.minRoom;
            	}

            	if (this.adult < this.guestRule.minAdult) {
            		this.adult = this.guestRule.minAdult;
            	}

            	if (this.child < this.guestRule.minChild) {
            		this.child = this.guestRule.minChild;
            	}

            	if (this.room > this.guestRule.maxRoom) {
            		this.room = this.guestRule.maxRoom;
            	}

            	if (this.adult > this.guestRule.maxAdult) {
            		this.adult = this.guestRule.maxAdult;
            	}

            	if (this.child > this.guestRule.maxChild) {
            		this.child = this.guestRule.maxChild;
            	}
            }


      },    

      created: function(){

      
      },

      watch:{
            room : function(){

            	this.guestValidation();
            	
            	if (this.adult < this.room) {
            		this.adult = this.room;
            	}


            	//update guest rule
            	this.guestRule.minAdult = this.room;
            	this.guestRule.maxAdult = this.room * 4;
            	this.guestRule.maxChild = this.room * 2;

            	this.guestValidation();

            },
            adult : function(){

            	this.guestValidation();

            },
            child : function(){

            	this.guestValidation();
            	
            }
      },

});