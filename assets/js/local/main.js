$(function(){


    function headerFixedTop() {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 50) {
            $('.navbar').addClass('fixed-top');
        } else {
            $('.navbar').removeClass('fixed-top');
        }

        console.log(scrollTop);
    };


    function productFilerFixedTop() {

        if ($('.card-product-filter').length > 0) {

             var scrollTop = $(window).scrollTop();
            if (scrollTop > 95) {
                $('.card-product-filter-section').addClass('filter-sticky');
            } else {
                $('.card-product-filter-section').removeClass('filter-sticky');
            }

        }

        return false;
       
    };

    headerFixedTop();
    productFilerFixedTop();


    $(window).on('scroll', function () {
        headerFixedTop();
        productFilerFixedTop();
    });


})
$(function(){

    
	//Slider
	$('.home-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots : false,
		centerMode: true,
		centerPadding : "21%",
		adaptiveHeight: true,
		autoplay: true,
		prevArrow: '<button type="button" class=" d-none d-sm-block slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class=" d-none d-sm-block slick-arrow slick-next"><i class="fa fa-chevron-right"></i></button>',
         responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              }
            }
        ]
	});


	 $('.product-slider-container').slick({
        slidesToShow: 5,
        slidesToScroll: 2,
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-chevron-right"></i></button>',
         responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              }
            }
        ]
    })

	 $('.category-slider-container').slick({
        slidesToShow: 6,
        slidesToScroll: 2,
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-chevron-right"></i></button>',
         responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              }
            }
        ]
    })


})