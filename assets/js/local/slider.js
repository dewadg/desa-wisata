$(function(){

    
	//Slider
	$('.home-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots : false,
		centerMode: false,
		adaptiveHeight: true,
		autoplay: true,
		prevArrow: '<button type="button" class=" d-none d-sm-block slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class=" d-none d-sm-block slick-arrow slick-next"><i class="fa fa-chevron-right"></i></button>',
         responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              }
            }
        ]
	});


	 $('.product-slider-container').slick({
        slidesToShow: 5,
        slidesToScroll: 2,
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-chevron-right"></i></button>',
         responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              },

            },
              {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              },
              
            }
        ]
    })

	 $('.category-slider-container').slick({
        slidesToShow: 6,
        slidesToScroll: 2,
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="fa fa-chevron-right"></i></button>',
         responsive: [
          {
              breakpoint: 768,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                centerMode:false,
              }
            }
        ]
    })


})